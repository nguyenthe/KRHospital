package com.example.hongngoc.webview.frag;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;
import com.example.hongngoc.webview.R;

/**
 * Created by HongNgoc on 9/23/2016.
 */
public class SampleFragmentPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
    final int PAGE_COUNT = 6;
    private String tabTitles[] = new String[] { "Tab1", "Tab2", "Tab3" };
    private int tabIcons[] = {R.drawable.icon_symptom,R.drawable.icon_since,R.drawable.icon_frequency,R.drawable.icon_translate};
    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            // Open HomeFragment.java
            case 0:
                TabFragment1 homeFragment = new TabFragment1();
                return homeFragment;
            // Open PlaceOrderFragment.java
            case 1:
                TabFragment2 groupsFragment = new TabFragment2();
                return groupsFragment;
            case 2:
                TabFragment3 callLogsFragment = new TabFragment3();
                return callLogsFragment;
            case 3:
                TabFragment4 tab4 = new TabFragment4();
                return tab4;
            case 4:
                TabFragment5 tabFragment5 = new TabFragment5();
                return tabFragment5;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    @Override
    public int getPageIconResId(int position) {
        return 0;
    }
}

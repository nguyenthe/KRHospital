package com.example.hongngoc.webview;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.handlerListView.ToolbarHidingOnScrollListener;
import com.example.hongngoc.webview.modelObject.ListItemAdapter;
import com.example.hongngoc.webview.modelObject.PartBody;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HongNgoc on 9/22/2016.
 */
public class MainGallaryBody extends ActionBarActivity {

    private static String content_part_body = "";
    private static String id_Part_Body = "";
    private ArrayList<PartBody>  arrayPartBody = new ArrayList<PartBody>();
    private RecyclerView recyclerView;
    private ListItemAdapter mAdapter;
    private static FragmentManager fragmentManager;

    //the images to display
    Integer[] imageIDs = {
            R.drawable.head,
            R.drawable.hand,
            R.drawable.foot,
            R.drawable.stomache,
            R.drawable.back,
            R.drawable.chest,
            R.drawable.neck,
            R.drawable.hip
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.body_gallary);
        // Note that Gallery view is deprecated in Android 4.1---
        Gallery gallery = (Gallery) findViewById(R.id.gallery1);
        gallery.setAdapter(new ImageAdapter(this));
        gallery.setSelection(1);
        gallery.setSpacing(1);
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, final int position, long id)
            {
                Toast.makeText(getBaseContext(),"pic" + (position + 1) + " selected",
                        Toast.LENGTH_SHORT).show();
                getDataSet(1);
                // display the images selected
                ImageView imageView = (ImageView) findViewById(R.id.image1);
                imageView.setImageResource(imageIDs[position]);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (position) {
                            case 0:
                                Toast.makeText(MainGallaryBody.this,"Head",Toast.LENGTH_LONG).show();
                                newIntent(MainListItem.class, getResources().getString(R.string.head));
                                //getDataSet(1);
                                break;
                            case 1:
                                // do something else
                                Toast.makeText(MainGallaryBody.this,"Foot ",Toast.LENGTH_LONG).show();
                                newIntent(MainListItem.class,getResources().getString(R.string.eye));
                                break;
                            case 2:
                                Toast.makeText(MainGallaryBody.this,"Touch Stomach ",Toast.LENGTH_LONG).show();
                                newIntent(MainListItem.class,getResources().getString(R.string.mouth));
                                break;
                            case 3:
                                Toast.makeText(MainGallaryBody.this,"Touch Hand ",Toast.LENGTH_LONG).show();
                                newIntent(MainPartBody.class,getResources().getString(R.string.hand));
                                break;

                            case 4:
                                Toast.makeText(MainGallaryBody.this,"Body",Toast.LENGTH_LONG).show();
                                newIntent(MainListItem.class,"mouth");
                                break;
                            case 5:
                                newIntent(MainListItem.class,"back");
                                break;
                        }
                    }
                });
            }
        });
    }

    public void newIntent(Class a,String content){
        Intent intent= new Intent(MainGallaryBody.this,a);
        intent.putExtra("message",content);
        startActivity(intent);
    }

    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void getDataSet(int idx) {
        ConnectionToGetData loader = new ConnectionToGetData();
        System.out.println("getDataSet from mainListItem : " + idx);
        AsyncTask<String, String, List<PartBody>> result = loader.execute("" + idx);
        try {
            mAdapter = new ListItemAdapter(this,
                    (ArrayList<PartBody>) result.get());
            recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
            // Let's use a grid with 2 columns.
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            recyclerView.setAdapter(mAdapter);
            View tabBar = findViewById(R.id.fake_tab);
            View coloredBackgroundView = findViewById(R.id.colored_background_view);
            View toolbarContainer = findViewById(R.id.toolbar_container);
            View toolbar = findViewById(R.id.toolbar);
            recyclerView.setOnScrollListener(new ToolbarHidingOnScrollListener(toolbarContainer, toolbar, tabBar, coloredBackgroundView));
            System.out.println("Ket qua Ket qua "+ result.toString());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class ConnectionToGetData extends AsyncTask<String,String,List<PartBody>> {
        @Override
        protected List<PartBody>  doInBackground(String... strings) {
            Intent intent = getIntent();
            content_part_body = intent.getStringExtra("message");
            String content = "";
            try{
                String url3 = Common.url_spring+":8080/getIdByName/head";
                Document document_getName = Jsoup.connect(url3).get();
                System.out.println("Name Name name " + document_getName.toString());
                Elements body_name = document_getName.select("body");
                id_Part_Body = body_name.text();
                System.out.println("Name Body Name Body  : " + id_Part_Body);

                //String url2 = "http://192.168.0.30:8080/listPartsById/"+id_Part_Body;
                String url2 = Common.url_spring+":8080/listPartsById/1";
                Document document_demo = Jsoup.connect(url2).ignoreContentType(true).get();
                System.out.println("spring data : " + document_demo.text());
                JSONArray obj = new JSONArray(document_demo.text());
                ArrayList<PartBody> list_category = new ArrayList<PartBody>();
                for(int i=0;i<obj.length();i++){
                    JSONObject object = obj.getJSONObject(i);
                    System.out.println("abc "+object.get("name_en"));
                    PartBody body = new PartBody();
                    body.setId_parent(Integer.parseInt(object.get("id_parent").toString()));
                    body.setId_part(Integer.parseInt(object.get("id_dp").toString()));
                    body.setName_en(object.get("name_en").toString());
                    body.setName_ko(object.get("name_ko").toString());
                    body.setImage(object.get("image").toString());
                    arrayPartBody.add(body);
                }
                return arrayPartBody;
            }catch(Exception e){
                e.printStackTrace();
            }
            //return arrayPartBody;
            return arrayPartBody;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private int itemBackground;
        public ImageAdapter(Context c)
        {
            context = c;
            // sets a grey background; wraps around the images
            TypedArray a =obtainStyledAttributes(R.styleable.MyGallery);
            itemBackground = a.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 0);
            //itemBackground = a.getResourceId(R.color.dot_dark_screen1,-);
            a.recycle();
        }
        // returns the number of images
        public int getCount() {
            return imageIDs.length;
        }
        // returns the ID of an item
        public Object getItem(int position) {
            return position;
        }
        // returns the ID of an item
        public long getItemId(int position) {
            return position;
        }
        // returns an ImageView view
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(context);
            imageView.setImageResource(imageIDs[position]);
            imageView.setLayoutParams(new Gallery.LayoutParams(300, 300));
            imageView.setBackgroundResource(itemBackground);
            return imageView;
        }
    }
}

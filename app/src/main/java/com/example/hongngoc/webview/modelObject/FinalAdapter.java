package com.example.hongngoc.webview.modelObject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.common.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by HongNgoc on 9/25/2016.
 */
public class FinalAdapter extends RecyclerView.Adapter<FinalAdapter.FinalViewHolder>{
    private List<Final> dataObjectList;
    private Context context; //Activity
    private  OnItemClickListener mItemClickListener;

    public class FinalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txtTitle;
        TextView txtSymptom;
        TextView txtSince;
        TextView txtFrequency;
        TextView txtDate;
        ImageView imageView;

        public FinalViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView)itemView.findViewById(R.id.news_title);
            txtSymptom = (TextView)itemView.findViewById(R.id.news_description);
            txtSince = (TextView)itemView.findViewById(R.id.txtSince);
            txtFrequency = (TextView)itemView.findViewById(R.id.time_fre);
            txtDate = (TextView)itemView.findViewById(R.id.news_time);
            imageView = (ImageView) itemView.findViewById(R.id.imageforResult);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(getPosition(),view);
            }
        }
    }

    public FinalAdapter(Context context,List<Final> list){
        this.dataObjectList = list;
        this.context = context;
    }

    @Override
    public FinalViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.final_layout, parent, false);
        FinalViewHolder pvh = new FinalViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(FinalViewHolder holder, final int position) {
        final Final obj = this.dataObjectList.get(position);
        holder.txtTitle.setText(obj.getTitle());
        holder.txtSymptom.setText(obj.getContent());
        holder.txtSince.setText(obj.getSince_time());
        holder.txtFrequency.setText(obj.getFrequency_time());
        holder.txtDate.setText(obj.getDate());
        Picasso
                .with(context)
                .load(Common.url_image + obj.getImage())
                .into(holder.imageView);
    }


    @Override
    public int getItemCount() {
        return this.dataObjectList.size();
    }

    public Final getItem(int position){
        return dataObjectList.get(position);
    }
    public void addData(List<Final> listOther){
        this.dataObjectList.addAll(listOther);
        System.out.println("totalSize: "+this.dataObjectList.size());
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        public void onItemClick(int position,View view);
    }

    public void SetOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}

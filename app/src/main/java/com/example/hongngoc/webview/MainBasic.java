package com.example.hongngoc.webview;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.translate.Translate;
import com.squareup.picasso.Picasso;

import java.util.Locale;

/**
 * Created by HongNgoc on 9/3/2016.
 */
public class MainBasic extends Activity implements View.OnClickListener {

    String arr[]={
            "Korean",
            "English"};

    private ImageView imageView;
    private Button btnDirectly;
    private Button btnSympom;
    private Locale myLocale;
    private Button btnSince;
    private Button btnFrequency;
    private Button btnResult;
    private Spinner spinner;
    private ImageButton btnBack;
    private static String content = "";
    private static String id_department="";
    private String time_since = "";
    private String time_frequency = "";
    private String content_part_body = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partbody);
        getEventView();
        getContentIntent();
        loadLocale();
        getContentTime();
    }

    public void getContentTime(){
        Intent intent = getIntent();
        time_since = intent.getStringExtra("time_since");
        if(time_since == null){
            System.out.println("Nothing time_since !!!!");
        }else
            System.out.println("Time Since : " + time_since);
        time_frequency = intent.getStringExtra("time_frequency");
        if(time_frequency == null) {
            System.out.println("Nothing time_frequency");
        }else
            System.out.println("Time Frequency : " + time_frequency);
    }

    public void getContentIntent(){
        Intent intent = getIntent();
        content = intent.getStringExtra("message");
        id_department = intent.getStringExtra("id_department");
        content_part_body = intent.getStringExtra("name_part");
        System.out.println("Content after get message : " + content);
        System.out.println("Id id department : " + id_department);
        System.out.println("Name department : " + content_part_body);
        Picasso
                .with(MainBasic.this)
                .load(Common.url_spring+":8080/" + content)
                .into(imageView);
    }

    public void getEventView(){
        imageView = (ImageView)findViewById(R.id.imageBody);
        btnDirectly = (Button)findViewById(R.id.btnDirectly);
        btnDirectly.setOnClickListener(this);
        btnSympom = (Button)findViewById(R.id.btnSym);
        btnSympom.setOnClickListener(this);
        btnSince = (Button)findViewById(R.id.btnSince);
        btnSince.setOnClickListener(this);
        btnFrequency = (Button)findViewById(R.id.btnFrequency);
        btnFrequency.setOnClickListener(this);
        btnResult = (Button)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(this);
        spinner = (Spinner)findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, arr);
        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new MyProcessEvent());
        btnBack = (ImageButton)findViewById(R.id.btnBackMainBasic);
        btnBack.setOnClickListener(this);
    }

    public void showImage(){
        imageView.setImageResource(R.drawable.bung);
    }

    @Override
    public void onClick(View view) {
        String lang = "en";
        switch (view.getId()){
            case R.id.btnDirectly:
                createIntent(Translate.class);
                break;
            case R.id.btnSym:
                System.out.println("Symptom Symptom Symptom !!!!");
                createIntent(MainHandlerListView.class);
                break;
            case R.id.btnSince:
                createIntent(MainSince.class);
                break;
            case R.id.btnFrequency:
                createIntent(MainFrequency.class);
                break;
            case R.id.btnResult:
                createIntent(MainResult.class);
                break;
            case R.id.btnBackMainBasic:
                newIntent(MainPartBody.class);
                break;
            default:
                break;
        }

    }

    public void changeLang(String lang)
    {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }


    public void saveLocale(String lang)
    {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }


    public void loadLocale()
    {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref,"");
        changeLang(language);
    }


    private void updateTexts()
    {
        btnSince.setText(R.string.since);
        btnFrequency.setText(R.string.frequency);
        btnResult.setText(R.string.result);
        btnDirectly.setText(R.string.directly);
        btnSympom.setText(R.string.symptom);
    }

    public void newIntent(Class a){
        Intent intent = new Intent(MainBasic.this,a);
        startActivity(intent);
    }

    public void createIntent(Class a){
        Intent intent = new Intent(MainBasic.this,a);
        intent.putExtra("message",content);
        intent.putExtra("id_department",id_department);
        intent.putExtra("name_part",content_part_body);
        intent.putExtra("time_since",time_since);
        intent.putExtra("time_frequency",time_frequency);
        startActivity(intent);
    }

    public class MyProcessEvent implements
            AdapterView.OnItemSelectedListener
    {
        //Khi có chọn lựa thì vào hàm này
        public void onItemSelected(AdapterView<?> arg0,
                                   View arg1,
                                   int arg2,
                                   long arg3) {
            //arg2 là phần tử được chọn trong data source
            changeLang(arr[arg2]);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }

    }
}

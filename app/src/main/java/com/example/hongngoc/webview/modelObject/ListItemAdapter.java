package com.example.hongngoc.webview.modelObject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.common.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.ObjectViewHolder> {
    private List<PartBody> dataObjectList;
    private Context context; //Activity
    private  OnItemClickListener mItemClickListener;

    public class ObjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textView1;
        ImageView imageView;
        public ObjectViewHolder(View itemView) {
            super(itemView);
            textView1 = (TextView)itemView.findViewById(R.id.textView_list_item);
            imageView = (ImageView) itemView.findViewById(R.id.image_list_item);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(getPosition(),view);
            }
        }
    }

    public ListItemAdapter(Context context,List<PartBody> list){
        this.dataObjectList = list;
        this.context = context;
    }

    @Override
    public ObjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_list_item, parent, false);
        ObjectViewHolder pvh = new ObjectViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ObjectViewHolder holder, final int position) {
        final PartBody obj = this.dataObjectList.get(position);
        holder.textView1.setText(obj.getName_ko());
        Picasso
                .with(context)
                .load(Common.url_image + obj.getImage())
                .into(holder.imageView);
    }


    @Override
    public int getItemCount() {
        return this.dataObjectList.size();
    }

    public PartBody getItem(int position){
        return dataObjectList.get(position);
    }
    public void addData(List<PartBody> listOther){
        this.dataObjectList.addAll(listOther);
        System.out.println("totalSize: "+this.dataObjectList.size());
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        public void onItemClick(int position,View view);
    }

    public void SetOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}

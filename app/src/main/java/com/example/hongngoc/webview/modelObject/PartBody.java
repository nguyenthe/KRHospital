package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/7/2016.
 */
public class PartBody {
    private int id_part;
    private int id_parent;
    private String name_en;
    private String name_ko;
    private String image;

    public int getId_part() {
        return id_part;
    }

    public void setId_part(int id_part) {
        this.id_part = id_part;
    }

    public int getId_parent() {
        return id_parent;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ko() {
        return name_ko;
    }

    public void setName_ko(String name_ko) {
        this.name_ko = name_ko;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "PartBody{" +
                "id_part=" + id_part +
                ", id_parent=" + id_parent +
                ", name_en='" + name_en + '\'' +
                ", name_ko='" + name_ko + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public PartBody(){}

    public PartBody(int id_part, int id_parent, String name_en, String name_ko, String image) {
        this.id_part = id_part;
        this.id_parent = id_parent;
        this.name_en = name_en;
        this.name_ko = name_ko;
        this.image = image;
    }
}

package com.example.hongngoc.webview.modelObject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.common.Common;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PartBodyAdapter extends ArrayAdapter<PartBody>{
    Context context; //Activity
    int layoutResourceId;
    ArrayList<PartBody> data=new ArrayList<PartBody>();
    public PartBodyAdapter(Context context, int layoutResourceId, ArrayList<PartBody> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ImageHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ImageHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.name_en);
            holder.contentKo = (TextView)row.findViewById(R.id.name_ko);
            holder.id_depart = (TextView)row.findViewById(R.id.id_dpartment);
            holder.imgIcon = (ImageView)row.findViewById(R.id.imagePartBody);
            row.setTag(holder);
        }
        else
        {
            holder = (ImageHolder)row.getTag();
        }
        PartBody body = data.get(position);
        holder.txtTitle.setText(body.getName_en());
        holder.contentKo.setText(body.getName_ko());
        holder.id_depart.setText(Integer.toString(body.getId_part()));
        Picasso
                .with(context)
                .load(Common.url_image + body.getImage())
                .into(holder.imgIcon);
        holder.id_depart.setVisibility(View.GONE);

        return row;
    }
    static class ImageHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView contentKo;
        TextView id_depart;
    }
}


package com.example.hongngoc.webview.frag;

/**
 * Created by HongNgoc on 9/23/2016.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hongngoc.webview.MainTab;
import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.modelObject.DBHelper;
import com.example.hongngoc.webview.modelObject.Symptom;
import com.example.hongngoc.webview.modelObject.SymptomAdapterUpdate;
import com.example.hongngoc.webview.modelObject.SymtomAdapter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class TabFragment1 extends Fragment implements View.OnClickListener{

    Context context; //Activity
    private ArrayList<Symptom>  arrayObject = null;
    private ArrayList<Symptom> list_content = new ArrayList<Symptom>();
    private ArrayAdapter<String>adapter = null;
    private RecyclerView mRecyclerView;
    private SymptomAdapterUpdate mAdapter;
    private SymtomAdapter m_fakeAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public ArrayList<String> list_content_update = new ArrayList<String>();
    public ArrayList<Symptom> list_content_update_2 = new ArrayList<Symptom>();
    ListView listView;
    ArrayAdapter<String> adapter_listview;
    Button btnList;
    Button btnBack;
    private DBHelper db;
    int mCurCheckPosition;
    String content_send_for_another_class = null;
    Symptom content_send_for_another_class_2;
    //final static String url_internet_local = "http://192.168.1.5/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container == null){
            return null;
        }
        context = container.getContext();
        db = new DBHelper(context);
        //getdata(savedInstanceState);
        View view = inflater.inflate(R.layout.cardview, container, false);
        System.out.println("Image : " + MainTab.image);
        System.out.println("Id of department : " + MainTab.id_dp);
        btnBack = (Button)view.findViewById(R.id.btnBackList);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //cai nay can thiet
        getDataSet(1);

        mRecyclerView.setNestedScrollingEnabled(false);
        handlerClickChoose(view);
        return view;
    }

    public void replaceFragments(Class fragmentClass,String content) {
        Fragment fragment = new Fragment();
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        bundle.putString("symptom",content);
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.idCardView, fragment).addToBackStack(null)
                .commit();
    }

    public static TabFragment1 newInstance(String param1, String param2){
        Bundle args = new Bundle();
        args.putString("image",param1);
        args.putString("content",param2);
        TabFragment1 fragment = new TabFragment1();
        fragment.setArguments(args);
        return fragment;
    }

    public void sendData(Class fragmentClass){
        System.out.println("Size list Symptom : " + list_content_update.size());
        if(list_content_update.size()==0){
            MainTab.myBundle.putString("abc","abc");
        }else {
            MainTab.myBundle.putStringArrayList("list_symptom",list_content_update);
        }

    }

    private void getDataSet(int idx) {
        ConnectionToGetData loader = new ConnectionToGetData();
        System.out.println("getDataSet: " + idx);
        AsyncTask<String, String, List<Symptom>> result = loader.execute("" + idx);
        try {
            //mai thao cai comment nay ra
            arrayObject = (ArrayList<Symptom>) result.get();
            System.out.println("Size demo  tabf1" + arrayObject.size());
            // specify an adapter (see also next example)
            mAdapter = new SymptomAdapterUpdate(arrayObject);
            mRecyclerView.setAdapter(mAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //handler click choose symptom
    public void handlerClickChoose(View view){
        btnList = (Button)view.findViewById(R.id.btnTest);
        btnBack.setVisibility(View.GONE);

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*System.out.println("Hello im here i need number : " + SymptomAdapterUpdate.list);
                Set uniqueEntries = new HashSet();
                for(int i=0;i<SymptomAdapterUpdate.list.size();i++){
                    list_content_update_2.add(arrayObject.get(SymptomAdapterUpdate.list.get(i)));
                }
                HashSet<Symptom> listToSet = new HashSet<Symptom>(list_content_update_2);
                //Creating Arraylist without duplicate values
                ArrayList<Symptom> listWithoutDuplicates = new ArrayList<Symptom>(listToSet);
                System.out.println("size of ArrayList without duplicates: " + listToSet.size()); //should print 3 becaues of duplicates Android removed
                //chu y cho nay*/

                for(int i=mRecyclerView.getChildCount()-1;i>=0;i--){
                    View v = mRecyclerView.getChildAt(i);
                    CheckBox chk = (CheckBox)v.findViewById(R.id.checkboxItem);
                    if(chk.isChecked()){
                        list_content_update_2.add(arrayObject.get(i));
                        System.out.println("array array : " + list_content.size());
                    }
                }

                System.out.println("Size list Symptom : " + list_content_update_2.size());
                m_fakeAdapter = new SymtomAdapter(list_content_update_2);
                mRecyclerView.setAdapter(m_fakeAdapter);
                btnBack.setVisibility(View.VISIBLE);
                btnList.setVisibility(View.GONE);
                mRecyclerView.setNestedScrollingEnabled(false);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnBack.setVisibility(View.GONE);
                btnList.setVisibility(View.VISIBLE);
                mAdapter = new SymptomAdapterUpdate(arrayObject);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setNestedScrollingEnabled(false);
            }
        });

        m_fakeAdapter = new SymtomAdapter(list_content_update_2);
        m_fakeAdapter.setOnItemLongClickListener(new SymtomAdapter.OnItemLongClickListener() {
            @Override
            public void onItemlongClick(final int position1, View v) {
                final int static_position = position1;
                if(list_content_update_2.size()==0){
                    content_send_for_another_class = list_content.get(position1).getContent_en();
                    Toast.makeText(context, "Long long content main list" + content_send_for_another_class, Toast.LENGTH_LONG).show();
                    replaceFragments(TabFragment4.class,content_send_for_another_class);
                    btnBack.setVisibility(View.VISIBLE);
                    btnList.setVisibility(View.GONE);
                }else {
                    Dialog d = new AlertDialog.Builder(context,AlertDialog.THEME_HOLO_LIGHT)
                            .setTitle("Choice Languge")
                            .setNegativeButton("Cancel", null)
                            .setItems(new String[]{"English","Korean"}, new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dlg, int position)
                                {
                                    if(position == 0)
                                    {
                                        content_send_for_another_class = list_content_update_2.get(static_position).getContent_en();
                                        replaceFragments(TabFragment4.class, content_send_for_another_class);
                                        btnBack.setVisibility(View.VISIBLE);
                                        btnList.setVisibility(View.GONE);
                                    }else{
                                        content_send_for_another_class = list_content_update_2.get(static_position).getContent_ko();
                                        replaceFragments(TabFragment4.class, content_send_for_another_class);
                                        btnBack.setVisibility(View.VISIBLE);
                                        btnList.setVisibility(View.GONE);
                                    }
                                }
                            })
                            .create();
                    d.show();

                }
            }
        });
    }

    public class ConnectionToGetData extends AsyncTask<String,String,List<Symptom>> {
        @Override
        protected List<Symptom>  doInBackground(String... strings) {

            String content = "";
            try{
                //demo demo
                //String url = "http://192.168.1.25:8983/solr/select?q=*:*%20&fq={!join%20from=id_parent%20to=id_parent}id_parent:1";
                String url = Common.apache_url+":8983/solr/collection1/select?q=id_dp%3A"+MainTab.id_dp+"&wt=json&indent=true";
                Document document1 = Jsoup.connect(url).get();
                Elements body = document1.select("body");
                System.out.println("The The The : " + body.text());
                String text_body = body.text().toString();
                System.out.println("TEXT TEXT : " + text_body);
                // contacts JSONArray

                JSONObject obj2 = new JSONObject(text_body);
                //System.out.println("Demo Demo Demo : " + obj);
                String demo = obj2.getString("response");
                System.out.println("Lenght : " + demo);

                JSONObject ob = new JSONObject(demo);
                System.out.println("OBOB"+ob);

                JSONArray jsonArray = ob.optJSONArray("docs");
                ArrayList<String> arrayList = new ArrayList<String>();
                arrayObject = new ArrayList<Symptom>();
                for(int i=0;i<jsonArray.length();i++){
                    System.out.println("iiiiiiiiiiiiiii");
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Symptom data = new Symptom();
                    data.setId_parent(Integer.parseInt(jsonObject.get("id_parent").toString()));
                    data.setId_dp(Integer.parseInt(jsonObject.get("id_dp").toString()));
                    data.setId_symptom(Integer.parseInt(jsonObject.get("id_symptom").toString()));
                    data.setContent_en(jsonObject.get("content_en").toString());
                    data.setContent_ko(jsonObject.get("content_ko").toString());
                    arrayObject.add(data);
                }

                for(int i=0;i<arrayObject.size();i++){
                    System.out.println("one by one element in array is : " + arrayObject.get(i).toString());
                }
                return arrayObject;
                //return null;
            }catch(Exception e){
                e.printStackTrace();
            }
            return arrayObject;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putStringArrayList("dataGotFromServer",list_content_update);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //list_content_update = savedInstanceState.getStringArrayList("dataGotFromServer");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /* Calendar c = Calendar.getInstance();
                System.out.println("Cureent time => " +c.getTime());
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
                String formatDate = df.format(c.getTime());
                System.out.println("List List Size Size is : " + list_content.size());
                for(int i=0;i<list_content.size();i++) {
                    Sym ob = new Sym();
                    ob.setId_dp(Integer.parseInt("1"));
                    ob.setId_parent(1);
                    ob.setContent(list_content.get(i).toString());
                    ob.setDate_info(formatDate);
                    db.insertTableSymptom(ob);
                }*/

}
package com.example.hongngoc.webview;

import android.app.Activity;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by HongNgoc on 8/30/2016.
 */
public class WebActivity extends Activity {

    private WebView webView;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        webView = (WebView) findViewById(R.id.webView);

        webView.setWebViewClient(new WebViewClient() {
                                    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                        handler.proceed();
                                    }
                                });

        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl("https://192.168.0.36:8080/");

        //String customHtml = "<html><body><h2>Greetings from JavaCodeGeeks</h2></body></html>";
        //webView.loadData(customHtml, "text/html", "UTF-8");

    }

}
package com.example.hongngoc.webview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by HongNgoc on 9/18/2016.
 */
public class MainStartup extends Activity implements View.OnClickListener {

    private Button imageStart;
    private Button imageGuide;
    private Button btnDirective;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutstart);
        getLayout();
    }

    public void getLayout(){
        imageGuide = (Button)findViewById(R.id.btnGuide);
        imageGuide.setOnClickListener(this);
        imageStart = (Button)findViewById(R.id.btnStart);
        imageStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnStart:
                newIntent(MainGallaryBody2.class);
                break;
            case R.id.btnGuide:
                newIntent(WelcomeActivity.class);
                break;
        }
    }

    public void newIntent(Class a){
        Intent intent = new Intent(MainStartup.this,a);
        startActivity(intent);
    }
}

package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/11/2016.
 */
public class ObjectReView {
    int id;
    String time_since;
    String time_frequency;
    String symptom;
    String name_part;
    String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime_since() {
        return time_since;
    }

    public void setTime_since(String time_since) {
        this.time_since = time_since;
    }

    public String getTime_frequency() {
        return time_frequency;
    }

    public void setTime_frequency(String time_frequency) {
        this.time_frequency = time_frequency;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getName_part() {
        return name_part;
    }

    public void setName_part(String name_part) {
        this.name_part = name_part;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ObjectReView(int id, String time_since, String time_frequency, String symptom, String name_part, String date) {
        this.id = id;
        this.time_since = time_since;
        this.time_frequency = time_frequency;
        this.symptom = symptom;
        this.name_part = name_part;
        this.date = date;
    }

    public ObjectReView(){}
}

package com.example.hongngoc.webview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.hongngoc.webview.modelObject.DBHelper;
import com.example.hongngoc.webview.modelObject.Sym;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by HongNgoc on 9/13/2016.
 */
public class Test extends Activity {

    private DBHelper db;
    private ListView listView;
    private ArrayAdapter<Sym> adapter;
    private ArrayList<Sym> list_Sym = new ArrayList<Sym>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(this);
        //setContentView(R.layout.test);
        //getViewFromLayout();
        //handler();
    }

    public String getDate(){
        Calendar c = Calendar.getInstance();
        System.out.println("Cureent time => " +c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
        String formatDate = df.format(c.getTime());
        return formatDate;
    }

    public void getViewFromLayout(){
        //listView = (ListView)findViewById(R.id.id_ListSymtom);
    }

    public void handler(){
        list_Sym = (ArrayList<Sym>) db.getAllSymtpmByIdPart(1,getDate());
        adapter=new ArrayAdapter<Sym>
                (Test.this,android.R.layout.simple_list_item_1,
                        list_Sym);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Sym item = (Sym) adapter.getItem(position);
                Dialog d = new AlertDialog.Builder(Test.this,AlertDialog.THEME_HOLO_LIGHT)
                        .setTitle("Symptom you selected!!!")
                        .setNegativeButton("Cancel", null)
                        .setItems(new String[]{item.getContent()}, new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dlg, int position)
                            {
                                if ( position == 0 )
                                {}
                            }
                        })
                        .create();
                d.show();
            }
        });
    }
}


/*

package com.example.hongngoc.webview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    private WebView webView;
    private Button btnSend;
    private ImageButton btnBack;
    private TextView tvInternet;
    ImageButton btnEye,btnStomache,btnHand,btnHead,imageTest;
    final String URL = "https://192.168.0.10:8080/";
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*tvInternet = (TextView)findViewById(R.id.tvInternet);
        isInternetOn();
        btnEye = (ImageButton)findViewById(R.id.imageEye);
        btnEye.setOnClickListener(this);
        btnStomache = (ImageButton)findViewById(R.id.imageStomache);
        btnStomache.setOnClickListener(this);
        btnHand = (ImageButton)findViewById(R.id.imageHand);
        btnHand.setOnClickListener(this);
        btnHead = (ImageButton)findViewById(R.id.imageHead);
        btnHead.setOnClickListener(this);
        imageTest = (ImageButton)findViewById(R.id.imageMouth);
        imageTest.setOnClickListener(this);
        btnBack = (ImageButton)findViewById(R.id.btnBackGuide);
        btnBack.setOnClickListener(this);*/
//Handler webView
/*webView = (WebView)findViewById(R.id.webViewMain);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
@Override public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                *//* Do whatever you need here *//*
        return super.onJsAlert(view, url, message, result);
        }

@Override
public void onPermissionRequest(PermissionRequest request) {
        // Show a grant or deny dialog to the user
        // On accept or deny call
        request.grant(request.getResources()) ;
        // or
        // request.deny()
        }
        });
        webView.setWebViewClient(new SSLTolerentWebViewClient());


        btnSend = (Button)findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.addJavascriptInterface(new LoginJsInterface(), "webviewScriptAPI");
        String fulljs = "javascript:(\n (function() {\n";
        fulljs += " window.onload = function({\n";
        fulljs += " webviewScriptAPI.onLoad();\n";
        fulljs += " };\n";
        fulljs += " })()\n";
        webView.loadUrl(fulljs);
        webView.loadUrl(URL);
        }
        });
        }

public void onLoadCompleted()
        {
        webView.loadUrl("javascript:document.getElementById('submit_button').style.backgroundColor='#3b3b3b'");

        webView.loadUrl("javascript:document.getElementById('submit_button').style.width='100%'");

        }
class LoginJsInterface
{
    @JavascriptInterface
    public void onLoad()
    {
        onLoadCompleted();
    }
}

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageStomache:
                Toast.makeText(this,"Stomache",Toast.LENGTH_LONG).show();
                newIntent(MainPartBody.class, getResources().getString(R.string.stomach));
                break;
            case R.id.imageEye:
                // do something else
                Toast.makeText(this,"Touch Eye ",Toast.LENGTH_LONG).show();
                newIntent(MainPartBody.class,getResources().getString(R.string.eye));
                break;
            case R.id.imageHand:
                Toast.makeText(this,"Touch Hand ",Toast.LENGTH_LONG).show();
                break;

            case R.id.imageHead:
                Toast.makeText(this,"Touch Head ",Toast.LENGTH_LONG).show();
                newIntent(MainPartBody.class,getResources().getString(R.string.head));
                break;

            case R.id.imageMouth:
                Toast.makeText(MainActivity.this,"Body",Toast.LENGTH_LONG).show();
                newIntent(Test.class,"mouth");
                break;
            case R.id.btnBackGuide:
                newIntent(MainStartup.class,"back");
        }
    }

    public void newIntent(Class a,String content){
        Intent intent= new Intent(MainActivity.this,a);
        intent.putExtra("message",content);
        startActivity(intent);
    }

private class SSLTolerentWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed(); // Ignore SSL certificate errors
    }

}

    //check internet
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet

            Toast.makeText(this, "Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            ////Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
            tvInternet.setText("Not Connected !!!");
            return false;
        }
        return false;
    }

private class MyWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        webView.loadUrl(url);
        return true;
    }*/

   /* @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return false;
    }

    @Override
    public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
        super.onReceivedClientCertRequest(view, request);
    }*/

    /*@Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                   SslError error) {
        super.onReceivedSslError(view, handler, error);
        handler.proceed();
    }
}
}*/


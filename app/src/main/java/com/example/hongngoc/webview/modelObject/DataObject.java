package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 8/30/2016.
 */
public class DataObject {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DataObject(){}

    public DataObject(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "DataObject{" +
                "content='" + content + '\'' +
                '}';
    }
}

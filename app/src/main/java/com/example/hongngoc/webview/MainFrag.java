package com.example.hongngoc.webview;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import com.example.hongngoc.webview.frag.SampleFragmentPagerAdapter;

/**
 * Created by HongNgoc on 9/23/2016.
 */
public class MainFrag extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagertab);

        /** Getting a reference to the ViewPager defined the layout file */
        ViewPager pager = (ViewPager) findViewById(R.id.vpPager);

        /** Getting fragment manager */
        FragmentManager fm = getSupportFragmentManager();

        /** Instantiating FragmentPagerAdapter */
        SampleFragmentPagerAdapter pagerAdapter = new SampleFragmentPagerAdapter(fm);
        /** Setting the pagerAdapter to the pager object */
        pager.setAdapter(pagerAdapter);

    }

}

package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/11/2016.
 */
public class Since {
    public int id_since;
    public String time_since;
    public int id_part;
    public String date_info;

    public int getId_since() {
        return id_since;
    }

    public void setId_since(int id_since) {
        this.id_since = id_since;
    }

    public String getTime_since() {
        return time_since;
    }

    public void setTime_since(String time_since) {
        this.time_since = time_since;
    }

    public String getDate_info() {
        return date_info;
    }

    public void setDate_info(String date_info) {
        this.date_info = date_info;
    }

    public int getId_part() {
        return id_part;
    }

    public void setId_part(int id_part) {
        this.id_part = id_part;
    }

    public Since(){}

    @Override
    public String toString() {
        return "Since{" +
                "id_since=" + id_since +
                ", time_since='" + time_since + '\'' +
                ", id_part=" + id_part +
                ", date_info='" + date_info + '\'' +
                '}';
    }

    public Since(int id_since, String time_since, int id_part, String date_info) {
        this.id_since = id_since;
        this.time_since = time_since;
        this.id_part = id_part;
        this.date_info = date_info;
    }
}

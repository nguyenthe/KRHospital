package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/13/2016.
 */
public class Sym {
    public int id;
    public int id_part;
    public int id_parent;
    public String content;
    public String date_info;

    public Sym(){}

    public Sym(int id_symptom, int id_dp, int id_parent, String content, String date_info) {
        this.id = id_symptom;
        this.id_part = id_dp;
        this.id_parent = id_parent;
        this.content = content;
        this.date_info = date_info;
    }

    public int getId_symptom() {
        return id;
    }

    public void setId_symptom(int id_symptom) {
        this.id = id_symptom;
    }

    public int getId_dp() {
        return id_part;
    }

    public void setId_dp(int id_dp) {
        this.id_part = id_dp;
    }

    public int getId_parent() {
        return id_parent;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate_info() {
        return date_info;
    }

    public void setDate_info(String date_info) {
        this.date_info = date_info;
    }

    @Override
    public String toString() {
        return "Sym{" +
                "id_symptom=" + id +
                ", id_dp=" + id_part +
                ", id_parent=" + id_parent +
                ", content='" + content + '\'' +
                ", date_info='" + date_info + '\'' +
                '}';
    }
}

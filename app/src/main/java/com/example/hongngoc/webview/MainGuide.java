package com.example.hongngoc.webview;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by HongNgoc on 9/17/2016.
 */
public class MainGuide extends Activity implements View.OnClickListener {

    ArrayList<String> functionNameList;
    private ImageButton imageBody;
    private ImageButton imagePart;
    private Button btnSince;
    private Button btnFre;
    private ImageButton btnSymptom;
    private ImageButton btnResult;
    private ImageButton btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutintroduce);
        // Get the reference of ListViewAnimals
        functionNameList = new ArrayList<String>();
        getLayout();
        getAnimalNames();

    }

    public void getLayout(){
        imageBody = (ImageButton)findViewById(R.id.imageButton);
        imageBody.setOnClickListener(this);
        imagePart = (ImageButton)findViewById(R.id.partBody);
        imagePart.setOnClickListener(this);
        btnSince = (Button)findViewById(R.id.iconTimeSince);
        btnSince.setOnClickListener(this);
        btnFre = (Button)findViewById(R.id.iconFrequency);
        btnFre.setOnClickListener(this);
        btnSymptom = (ImageButton)findViewById(R.id.imageSymptom);
        btnSymptom.setOnClickListener(this);
        btnResult = (ImageButton)findViewById(R.id.ImageResult);
        btnResult.setOnClickListener(this);
        btnBack = (ImageButton)findViewById(R.id.btnBackGuide);
        btnBack.setOnClickListener(this);
    }

    void getAnimalNames()
    {
        functionNameList.add("Choice main department on body .Example : head,chest,hand,leg,neck");
        functionNameList.add("Choice part that you feel problem.Example : eye , mouth,teeth,....");
        functionNameList.add("Choice time when you sick.1,firstly you check into hours or day or month 2,you choice time when you sick");
        functionNameList.add("Choice frequency when you sick by hours or day ");
        functionNameList.add("Choice symptom by check into square that you matched");
        functionNameList.add("Press result when you finish progressing");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageButton:
                alert(0);
                break;
            case R.id.partBody:
                alert(1);
                break;
            case R.id.iconTimeSince:
                alert(2);
                break;
            case R.id.iconFrequency:
                alert(3);
                break;
            case R.id.imageSymptom:
                alert(4);
                break;
            case R.id.ImageResult:
                alert(5);
                break;
            case R.id.btnBackGuide:
                newIntent(MainStartup.class);
                break;

        }
    }

    public void newIntent(Class a){
        Intent intent = new Intent(MainGuide.this,a);
        startActivity(intent);
    }


    public void alert(int position){
        String selectedContent=functionNameList.get(position);
        // Show The Dialog with Selected SMS
        AlertDialog dialog = new AlertDialog.Builder(MainGuide.this).create();
        dialog.setTitle("Title : ");
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setMessage(selectedContent);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        return;
                    }
                });
        dialog.show();
    }
}

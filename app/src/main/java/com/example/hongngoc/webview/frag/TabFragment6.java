package com.example.hongngoc.webview.frag;

/**
 * Created by HongNgoc on 9/23/2016.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.modelObject.Final;
import com.example.hongngoc.webview.modelObject.FinalAdapter;

import java.util.ArrayList;

public class TabFragment6 extends Fragment {
    Context context; //Activity
    String value;
    TextView news_description;
    TextView news_title;
    TextView news_time;
    private ArrayList<Final>  arrayPartBody = new ArrayList<Final>();
    private RecyclerView recyclerView;
    private FinalAdapter mAdapter;
    private ArrayList<String> list_symptom = new ArrayList<String>();
    private static String time_since;
    private static String time_frequency;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //define context for Activity
        /*list_symptom = MainTab.myBundle.getStringArrayList("list_symptom");
        if(list_symptom.size() == 0){
            System.out.println("Nothing list symptom");
        }else{
            System.out.println("array symptom into tab6" + list_symptom.size());
            for(int i=0;i<list_symptom.size();i++){
                Final object = new Final();
                object.setContent(list_symptom.get(i));
                arrayPartBody.add(object);
            }
            System.out.println("List symptom from Tab6 : " + arrayPartBody.size());
        }
        time_since = MainTab.myBundle.getString("time_since");
        System.out.println("Time_since"+ time_since);
        time_frequency = MainTab.myBundle.getString("time_frequency");
        System.out.println("Time_frequency : " + time_frequency);*/
        context = container.getContext();
        View view = inflater.inflate(R.layout.final_layout, container, false);
        getContentLayout(view);

        return view;
    }

    public void getContentLayout(View view){
        news_title = (TextView)view.findViewById(R.id.news_title);
        news_description = (TextView)view.findViewById(R.id.news_description);
        news_time = (TextView)view.findViewById(R.id.news_time);
    }

    public static TabFragment6 newInstance(String text){
        TabFragment6 f = new TabFragment6();
        Bundle b = new Bundle();
        b.putString("msg","this is from tab1");
        f.setArguments(b);
        return f;
    }


}
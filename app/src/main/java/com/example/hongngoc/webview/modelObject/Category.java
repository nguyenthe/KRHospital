package com.example.hongngoc.webview.modelObject;

public class Category {
    private int id_parent;
    private String name_en;
    private String name_ko;

    public int getId_parent() {
        return id_parent;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ko() {
        return name_ko;
    }

    public void setName_ko(String name_ko) {
        this.name_ko = name_ko;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id_parent=" + id_parent +
                ", name_en='" + name_en + '\'' +
                ", name_ko='" + name_ko + '\'' +
                '}';
    }
}

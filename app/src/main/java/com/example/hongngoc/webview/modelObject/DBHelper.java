package com.example.hongngoc.webview.modelObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "mydb.db";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_TIME_SINCE = "time_since";
    public static final String CONTACTS_COLUMN_TIME_FREQUENCY = "time_frequency";
    public static final String CONTACTS_COLUMN_NAME_PART = "name_part";
    public static final String CONTACTS_COLUMN_DATE = "date_info";

    //table since_time
    public static final String CONTACTS_TABLE_NAME_SINCE = "tbl_since";
    public static final String CONTACTS_COLUMN_ID_SINCE = "id";
    public static final String CONTACTS_COLUMN_ID_PART = "id_part";

    // table frequency_time
    public static final String CONTACTS_TABLE_NAME_FREQUENCY = "tbl_frequency";
    public static final String CONTACTS_COLUMN_ID_FRE = "id";

    // table symptom
    public static final String CONTACTS_TABLE_NAME_SYMPTOM = "tbl_symptom";
    public static final String CONTACTS_COMLUMN_ID_SYMPTOM = "id";
    public static final String CONTACTS_COLUMN_ID_PARENT = "id_parent";
    public static final String CONTACTS_COLUMN_CONTENT = "content";

    private HashMap hp;

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "create table tbl_since " +
                        "(id integer primary key AUTOINCREMENT NOT NULL,time_since text,id_part integer,date_info text)"
        );

        db.execSQL(
                "create table tbl_frequency " +
                        "(id integer primary key AUTOINCREMENT NOT NULL,time_frequency text,id_part integer,date_info text)"
        );

        db.execSQL(
                "create table tbl_symptom " +
                        "(id integer primary key AUTOINCREMENT NOT NULL,id_part integer,id_parent integer,content text,date_info text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tbl_since");
        db.execSQL("DROP TABLE IF EXISTS tbl_frequency");
        db.execSQL("DROP TABLE IF EXISTS tbl_symptom");
        onCreate(db);
    }

    ///////////////////////////////////////tbl_since
    public boolean insertTableSince(Since ob)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //contentValues.put(CONTACTS_COLUMN_ID,ob.id);
        contentValues.put(CONTACTS_COLUMN_TIME_SINCE,ob.time_since);
        contentValues.put(CONTACTS_COLUMN_ID_PART,ob.id_part);
        contentValues.put(CONTACTS_COLUMN_DATE,ob.date_info);
        db.insert("tbl_since", null, contentValues);
        return true;
    }

    public Cursor getOneSinceFromTableSince(int id_part,String date){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from tbl_since where id_part="+id_part +" AND date_info like'%"+date+"%'ORDER BY ID DESC",null );
        return res;
    }

    public Cursor getOneSinceLastIndex(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from tbl_since order by id DESC",null );
        return res;
    }


    ///////////////////////////////////////tbl_frequency

    public boolean insertTableFrequency(Frequency ob)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_TIME_FREQUENCY,ob.time_frequency);
        contentValues.put(CONTACTS_COLUMN_ID_PART,ob.id_part);
        contentValues.put(CONTACTS_COLUMN_DATE,ob.date_info);
        db.insert("tbl_frequency", null, contentValues);
        return true;
    }

    public Cursor getOneSinceFromTableFrequency(int id_part,String date){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from tbl_frequency where id_part="+id_part +" AND date_info like'%"+date+"%'  ORDER BY ID DESC",null );
        return res;
    }

    public Cursor getOneFrequencyLastIndex(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from tbl_frequency order by id DESC",null );
        return res;
    }

    public Cursor getOneFrequency(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from tbl_frequency",null );
        return res;
    }

    //This is function for counting rows in 1 table
    public int numberOfRows(String name_table){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db,name_table);
        return numRows;
    }
    // symptom//////////////////////

    public boolean insertTableSymptom(Sym ob)
    {
        //id integer primary key AUTOINCREMENT NOT NULL,id_part integer,id_parent integer,content text,date_info text
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id_part",ob.id_part);
        contentValues.put("id_parent",ob.id_parent);
        contentValues.put("content",ob.content);
        contentValues.put("date_info",ob.date_info);
        db.insert("tbl_symptom", null, contentValues);
        return true;
    }

    // Getting All Symptom by id of department
    public List<Sym> getAllSymtpmByIdPart(int id_part,String date) {
        List<Sym> contactList = new ArrayList<Sym>();
    // Select All Query
        String selectQuery = "SELECT * FROM tbl_symptom where id_part="+id_part+" AND date_info like'%"+date+"%'" ;
        System.out.println("Query symtptom list by day : " + selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Sym symptom = new Sym();
                symptom.setId_symptom(Integer.parseInt(cursor.getString(0)));
                symptom.setId_dp(cursor.getInt(1));
                symptom.setId_parent(cursor.getInt(2));
                symptom.setContent(cursor.getString(3));
                symptom.setDate_info(cursor.getString(4));
                contactList.add(symptom);
            } while (cursor.moveToNext());
        }
    // close inserting data from database
        db.close();
    // return contact list
        return contactList;
    }

    //get all list symptom
    // Getting All Symptom by id of department
    public List<Sym> getAllSymtpm() {
        List<Sym> contactList = new ArrayList<Sym>();
        // Select All Query
        String selectQuery = "SELECT * FROM tbl_symptom";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Sym symptom = new Sym();
                symptom.setId_symptom(Integer.parseInt(cursor.getString(0)));
                symptom.setId_dp(cursor.getInt(1));
                symptom.setId_parent(cursor.getInt(2));
                symptom.setContent(cursor.getString(3));
                symptom.setDate_info(cursor.getString(4));
                contactList.add(symptom);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return contact list
        return contactList;
    }
}

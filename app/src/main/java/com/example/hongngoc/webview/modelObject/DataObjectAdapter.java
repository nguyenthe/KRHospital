package com.example.hongngoc.webview.modelObject;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hongngoc.webview.MainHandlerListView;
import com.example.hongngoc.webview.R;

import java.util.ArrayList;
import java.util.List;

public class DataObjectAdapter extends RecyclerView.Adapter<DataObjectAdapter.DataObjectViewHolder>{

    private List<Symptom> dataObjectList;
    private Context mContext;

   private ArrayList<String> listContent = new ArrayList<String>();

    public DataObjectAdapter(Context ctx){
        this.dataObjectList = new ArrayList<>();
        this.mContext = ctx;
    }

    @Override
    public DataObjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_choose, parent, false);
        DataObjectViewHolder pvh = new DataObjectViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(DataObjectViewHolder holder, final int position) {
        final Symptom obj = this.dataObjectList.get(position);
        holder.textView1.setText(""+obj.getContent_en());

        //in some case ,it will prevent unwanted situations
        //holder.checkboxItem.setChecked(true);

        //if true, your checkbox will be selected ,else unselected.
        /*holder.checkboxItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()) {
                    compoundButton.setChecked(true);
                    try{
                        listContent.add(dataObjectList.get(position).getContent());
                        for(int i=0;i<listContent.size();i++){
                            System.out.println("Size Size : " + listContent.size());
                            System.out.println("content content size size : " + listContent.get(i));
                        }
                        //setListContent(listContent);
                        Intent intent = new Intent();
                        intent.putStringArrayListExtra("test",listContent);
                        mContext.startActivity(intent);

                    }catch(Exception e){
                        System.out.println("Nothing"+e.toString());
                    }
                }else{
                    Toast.makeText(mContext,"User has no choose nothing",Toast.LENGTH_LONG).show();
                }
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return this.dataObjectList.size();
    }

    public void addData(List<Symptom> listOther){
        this.dataObjectList.addAll(listOther);
        System.out.println("totalSize: "+this.dataObjectList.size());
        notifyDataSetChanged();
    }

    public static class DataObjectViewHolder extends RecyclerView.ViewHolder {
        TextView textView1;
        CheckBox checkboxItem;
        DataObjectViewHolder(View itemView) {
            super(itemView);
            textView1 = (TextView)itemView.findViewById(R.id.contentSymptom);
            checkboxItem = (CheckBox)itemView.findViewById(R.id.checkboxItem);
        }
    }

}

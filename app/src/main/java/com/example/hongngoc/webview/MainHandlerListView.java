package com.example.hongngoc.webview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.frag.TabFragment1;
import com.example.hongngoc.webview.handlerListView.EndlessRecyclerViewScrollListener;
import com.example.hongngoc.webview.modelObject.DBHelper;
import com.example.hongngoc.webview.modelObject.DataObjectAdapter;
import com.example.hongngoc.webview.modelObject.Symptom;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class MainHandlerListView extends Activity implements View.OnClickListener{
    private TextView textView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int current;
    private Button btnTotal;
    private ImageButton btnBack;
    private ArrayList<Symptom>  arrayObject = null;
    private ArrayAdapter<String> adapter = null;
    private ArrayList<String> list_content = new ArrayList<String>();
    ListView listView;
    private static String content_part_body = "";
    private static String id_partment = "";
    private static String time_since = "";
    private static String time_frequency = "";
    private static String id_department = "";
    DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this;
        //Don't touch
        db = new DBHelper(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cardview);
        current = 1;
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        textView = (TextView)findViewById(R.id.textView);
        //listView = (ListView)findViewById(R.id.listItem1);
        //btnBack = (ImageButton)findViewById(R.id.btnBackbBsic);
        //btnBack.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        //set Layout vertical recyclerView
        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                customLoadMoreDataFromApi(page);
            }
        });
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new DataObjectAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        getDataSet(1);
        ConnectionToGetData weatherConnection = new ConnectionToGetData();
        handlerTotal();
    }

    public void customLoadMoreDataFromApi(int page) {
        // Send an API request to retrieve appropriate data using the offset value as a parameter.
        //  --> Deserialize API response and then construct new objects to append to the adapter
        //  --> Notify the adapter of the changes
        //getDataSet(current++);
    }

    private void getDataSet(int idx) {
        ConnectionToGetData loader = new ConnectionToGetData();
        System.out.println("getDataSet: " + idx);
        AsyncTask<String, String, List<Symptom>> result = loader.execute("" + idx);

        try {
            ((DataObjectAdapter) mAdapter).addData(result.get());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
            Intent intent = new Intent(MainHandlerListView.this,MainBasic.class);
            intent.putExtra("time_since",time_since);
            intent.putExtra("time_frequency",time_frequency);
            intent.putExtra("id_department",id_partment);
            startActivity(intent);

    }

    public class ConnectionToGetData extends AsyncTask<String,String,List<Symptom>>{
        @Override
        protected List<Symptom>  doInBackground(String... strings) {
            Intent intent = getIntent();
            content_part_body = intent.getStringExtra("name_part");
            id_partment = intent.getStringExtra("id_department");
            time_since = intent.getStringExtra("time_since");
            time_frequency = intent.getStringExtra("time_frequency");
            System.out.println("Main Handler ListView has Id departMent : " + id_partment + " since :" +time_since +
                    "frequency:" + time_frequency+
                    "name_part"+content_part_body);
            String content = "";
            try{
                //demo demo
                //String url = "http://192.168.1.25:8983/solr/select?q=*:*%20&fq={!join%20from=id_parent%20to=id_parent}id_parent:1";
                String url = Common.apache_url+":8983/solr/collection1/select?q=id_dp%3A"+id_partment+"&wt=json&indent=true";
                Document document1 = Jsoup.connect(url).get();
                Elements body = document1.select("body");
                System.out.println("The The The : " + body.text());
                String text_body = body.text().toString();
                System.out.println("TEXT TEXT : " + text_body);
                // contacts JSONArray

                JSONObject obj2 = new JSONObject(text_body);
                //System.out.println("Demo Demo Demo : " + obj);
                String demo = obj2.getString("response");
                System.out.println("Lenght : " + demo);

                JSONObject ob = new JSONObject(demo);
                System.out.println("OBOB"+ob);

                JSONArray jsonArray = ob.optJSONArray("docs");
                ArrayList<String> arrayList = new ArrayList<String>();
                arrayObject = new ArrayList<Symptom>();
                for(int i=0;i<jsonArray.length();i++){
                    System.out.println("iiiiiiiiiiiiiii");
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Symptom data = new Symptom();
                    data.setId_parent(Integer.parseInt(jsonObject.get("id_parent").toString()));
                    data.setId_dp(Integer.parseInt(jsonObject.get("id_dp").toString()));
                    data.setId_symptom(Integer.parseInt(jsonObject.get("id_symptom").toString()));
                    data.setContent_en(jsonObject.get("content_en").toString());
                    data.setContent_ko(jsonObject.get("content_ko").toString());
                    arrayObject.add(data);
                }

                for(int i=0;i<arrayObject.size();i++){
                    System.out.println("one by one element in array is : " + arrayObject.get(i).toString());
                }

                return arrayObject;
                //return null;
            }catch(Exception e){
                e.printStackTrace();
            }
            return arrayObject;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void handlerTotal(){
        //btnTotal = (Button)findViewById(R.id.btnTotal);
        /*btnTotal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                for(int i=mRecyclerView.getChildCount()-1;i>=0;i--){
                    View v = mRecyclerView.getChildAt(i);
                    CheckBox chk = (CheckBox)v.findViewById(R.id.checkboxItem);
                    if(chk.isChecked()){
                        list_content.add(arrayObject.get(i).getContent_en());
                        System.out.println("array array : " + list_content.size());
                    }
                }
                adapter=new ArrayAdapter<String>
                        (MainHandlerListView.this,android.R.layout.simple_list_item_1,
                                list_content);
                listView.setAdapter(adapter);
                mRecyclerView.setVisibility(View.GONE);
                Calendar c = Calendar.getInstance();
                System.out.println("Cureent time => " +c.getTime());
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
                String formatDate = df.format(c.getTime());
                System.out.println("List List Size Size is : " + list_content.size());
                for(int i=0;i<list_content.size();i++) {
                    Sym ob = new Sym();
                    ob.setId_dp(Integer.parseInt(id_partment));
                    ob.setId_parent(1);
                    ob.setContent(list_content.get(i).toString());
                    ob.setDate_info(formatDate);
                    db.insertTableSymptom(ob);
                }
            }
        });*/
    }

}

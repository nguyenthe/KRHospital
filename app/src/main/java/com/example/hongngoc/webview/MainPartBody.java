package com.example.hongngoc.webview;

/**
 * Created by HongNgoc on 9/7/2016.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.modelObject.PartBody;
import com.example.hongngoc.webview.modelObject.PartBodyAdapter;
import com.example.hongngoc.webview.modelObject.Symptom;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainPartBody extends Activity implements View.OnClickListener {

    private Button btnTotal;
    private ImageButton btnBack;
    private ArrayList<PartBody>  arrayPartBody = new ArrayList<PartBody>();
    ListView listView;
    private static String content_part_body = "";
    private String id_Part_Body = "";
    PartBodyAdapter adapter;
    private ArrayList<Symptom>  arrayObject = null;
    //define Spinner
    String arr[]={
            "Korean",
            "English"};
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this;
        //Don't touch
        super.onCreate(savedInstanceState);
        Locale locale = new Locale("en_US");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config, null);
        setContentView(R.layout.mainviewparts);
        getAllView();
        ConnectionToGetData weatherConnection = new ConnectionToGetData();
        getDataSet(1);
        handlerOnePart();
    }

    public void getAllView(){
        btnBack = (ImageButton)findViewById(R.id.btnBackMainPartBody);
        btnBack.setOnClickListener(this);
        listView = (ListView)findViewById(R.id.listViewParts);
        spinner = (Spinner)findViewById(R.id.spinner_part);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, arr);
        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new MyProcessEvent());
    }

    private void getDataSet(int idx) {
        ConnectionToGetData loader = new ConnectionToGetData();
        System.out.println("getDataSet: " + idx);
        AsyncTask<String, String, List<PartBody>> result = loader.execute("" + idx);
        try {
            adapter = new PartBodyAdapter(this,R.layout.itemviewpart,
                    (ArrayList<PartBody>) result.get());
            listView.setAdapter(adapter);
            System.out.println("Ket qua Ket qua "+ result.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBackMainPartBody:
                newIntent(MainActivity.class,"");
                break;
        }
    }

    public class ConnectionToGetData extends AsyncTask<String,String,List<PartBody>>{
        @Override
        protected List<PartBody>  doInBackground(String... strings) {
            Intent intent = getIntent();
            content_part_body = intent.getStringExtra("message");
            String content = "";
            try{
                //String url3 = "http://192.168.0.36:8080/getIdByName/"+content_part_body;
                String url3 = Common.url_spring+":8080/getIdByName/head";
                Document document_getName = Jsoup.connect(url3).get();
                System.out.println("Name Name name " + document_getName.toString());
                Elements body_name = document_getName.select("body");
                id_Part_Body = body_name.text();
                System.out.println("Name Body Name Body  : " + id_Part_Body);

                //String url2 = "http://192.168.0.36:8080/listPartsById/"+id_Part_Body;
                String url2 = Common.url_spring+":8080/listPartsById/1";
                Document document_demo = Jsoup.connect(url2).ignoreContentType(true).get();
                System.out.println("spring data : " + document_demo.text());
                JSONArray obj = new JSONArray(document_demo.text());
                ArrayList<PartBody> list_category = new ArrayList<PartBody>();
                for(int i=0;i<obj.length();i++){
                    JSONObject object = obj.getJSONObject(i);
                    System.out.println("abc "+object.get("name_en"));
                    PartBody body = new PartBody();
                    body.setId_parent(Integer.parseInt(object.get("id_parent").toString()));
                    body.setId_part(Integer.parseInt(object.get("id_dp").toString()));
                    body.setName_en(object.get("name_en").toString());
                    body.setName_ko(object.get("name_ko").toString());
                    body.setImage(object.get("image").toString());
                    arrayPartBody.add(body);
                }

                return arrayPartBody;
            }catch(Exception e){
                e.printStackTrace();
            }
            //return arrayPartBody;
            return arrayPartBody;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void handlerOnePart(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                PartBody item = (PartBody) adapter.getItem(position);
                Toast.makeText(MainPartBody.this,item.getName_en().toString(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainPartBody.this,MainBasic.class);
                intent.putExtra("message",item.getImage());
                intent.putExtra("id_department",Integer.toString(item.getId_part()));
                intent.putExtra("name_part",content_part_body);
                startActivity(intent);
            }
        });
    }

    public void newIntent(Class a,String content){
        Intent intent= new Intent(MainPartBody.this,a);
        intent.putExtra("message",content);
        startActivity(intent);
    }

    public class MyProcessEvent implements AdapterView.OnItemSelectedListener
    {
        //Khi có chọn lựa thì vào hàm này
        public void onItemSelected(AdapterView<?> arg0, View arg1, int position,long arg3) {
            //arg2 là phần tử được chọn trong data source
           Toast.makeText(MainPartBody.this,arr[position],Toast.LENGTH_LONG).show();
            if(arr[position] == "English"){
                for(int i=listView.getChildCount()-1;i>=0;i--){
                    View v = listView.getChildAt(i);
                    TextView name_ko = (TextView) v.findViewById(R.id.name_ko);
                    name_ko.setVisibility(v.GONE);
                }
            }
            /*switch(position){
                case arr[position]:
                    Toast.makeText(MainPartBody.this,"Korean",Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(MainPartBody.this,"Engish",Toast.LENGTH_LONG).show();
                    *//*for(int i=listView.getChildCount()-1;i>=0;i--){
                        View v = listView.getChildAt(i);
                        TextView name_ko = (TextView) v.findViewById(R.id.name_ko);
                        name_ko.setVisibility(v.GONE);
                    }*//*
                    break;
            }*/
            //Toast.makeText(MainPartBody.this,"hi Im " + arr[arg2],Toast.LENGTH_LONG).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }

    }

}

package com.example.hongngoc.webview.frag;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hongngoc.webview.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URLEncoder;

/**
 * Created by HongNgoc on 10/6/2016.
 */
public class TabFragment7 extends Fragment {

    Context context; //Activity
    EditText editText;
    TextView tvContent;
    Button btnTranslteEnglish;
    Button btnKorean;
    String content = "";
    String result_language;
    String content_after_translate;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        View view = inflater.inflate(R.layout.test_api, container, false);
        editText = (EditText)view.findViewById(R.id.inputContent);
        tvContent = (TextView)view.findViewById(R.id.content_translate);
        btnKorean = (Button)view.findViewById(R.id.Korean);
        btnKorean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                content = editText.getText().toString();
                AsytaskTranslate demo = new AsytaskTranslate();
                demo.execute(content);
            }
        });
        btnTranslteEnglish = (Button)view.findViewById(R.id.btnEnglish);
        btnTranslteEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //content = editText.getText().toString();
                AsytaskTranslateKorean demo1 = new AsytaskTranslateKorean();
                demo1.execute(editText.getText().toString());
            }
        });
        return view;
    }

    public class CheckDetectionLanguage extends AsyncTask<String,String,String>{
        @Override
        protected String doInBackground(String... strings) {
            try {
                String result_voice = strings[0];
                //detection language
                String check_language = "https://www.googleapis.com/language/translate/v2/detect?key=AIzaSyCStJnRPJTip-UhZHHSz8ly8QDoV0ahACs&q="+result_voice;
                Document document_getName_1 = Jsoup.connect(check_language).ignoreContentType(true).get();
                System.out.println("format html " + document_getName_1.toString());
                Elements body_name_1 = document_getName_1.select("body");

                String text_body_1 = body_name_1.text().toString();
                System.out.println("TEXT TEXT : " + text_body_1);

                JSONObject obj2_1 = new JSONObject(text_body_1);
                String demo_1 = obj2_1.getString("data");
                System.out.println("data from json : " + demo_1);

                JSONObject obj3_1 = new JSONObject(demo_1);
                String demo1_1 = obj3_1.getString("detections");

                JSONArray jsonArray_1 = obj3_1.getJSONArray("detections");
                System.out.println("aaaaa111" + jsonArray_1.length());

                for (int i = 0; i < jsonArray_1.length(); i++) {
                    JSONArray jsonArray = jsonArray_1.getJSONArray(0);
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        result_language = jsonObject.getString("language");
                        System.out.println("AAAAA translate translate : " + result_language);
                        publishProgress(result_language);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            //return arrayPartBody;
            return result_language;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Toast.makeText(context,"AAAAAAAAAAAAAAAA"+values[0],Toast.LENGTH_LONG).show();
            if(values[0].equals("und")){
                Toast.makeText(context,"Sorry We cant understading !!!",Toast.LENGTH_LONG).show();
            }
            AsytaskTranslate an = new AsytaskTranslate();
            an.execute(values[0]);
        }
    }

    public class AsytaskTranslate extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... strings) {
            String content_tr = strings[0];
            try {
                System.out.println("Content will handler translate  ...!!!" + content_tr);
                String url3 = "https://www.googleapis.com/language/translate/v2?key=AIzaSyCStJnRPJTip-UhZHHSz8ly8QDoV0ahACs&q="+content_tr+"&source=en&target=ko";
                Document document_getName = Jsoup.connect(url3).ignoreContentType(true).get();
                System.out.println("format html " + document_getName.toString());
                Elements body_name = document_getName.select("body");

                String text_body = body_name.text().toString();
                System.out.println("TEXT TEXT : " + text_body);

                JSONObject obj2 = new JSONObject(text_body);
                String demo = obj2.getString("data");
                System.out.println("data from json : " + demo);

                JSONObject obj3 = new JSONObject(demo);
                String demo1 = obj3.getString("translations");

                JSONArray jsonArray = obj3.getJSONArray("translations");
                System.out.println("aaaaa" + jsonArray.length());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    content_after_translate = jsonObject.getString("translatedText");
                    System.out.println("AAAAA " + content_after_translate);
                    publishProgress(content_after_translate);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String value = values[0];
            tvContent.setText(value);
        }

    }

    //english
    public class AsytaskTranslateKorean extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            String content_tr = strings[0];
            System.out.println("Input start translate from korean to english!!! " + content_tr);
            try {
                System.out.println("Content will handler translate  ...!!!" + content_tr);
                String encodedText = URLEncoder.encode(content_tr, "UTF-8");
                String url3 = "https://www.googleapis.com/language/translate/v2?key=AIzaSyCStJnRPJTip-UhZHHSz8ly8QDoV0ahACs&q="+encodedText +"&source=ko&target=en";
                Document document_getName = Jsoup.connect(url3).ignoreContentType(true).get();
                System.out.println("format html " + document_getName.toString());
                Elements body_name = document_getName.select("body");

                String text_body = body_name.text().toString();
                System.out.println("TEXT TEXT : " + text_body);

                JSONObject obj2 = new JSONObject(text_body);
                String demo = obj2.getString("data");
                System.out.println("data from json : " + demo);

                JSONObject obj3 = new JSONObject(demo);
                String demo1 = obj3.getString("translations");

                JSONArray jsonArray = obj3.getJSONArray("translations");
                System.out.println("aaaaa" + jsonArray.length());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    content_after_translate = jsonObject.getString("translatedText");
                    System.out.println("AAAAA " + content_after_translate);
                    publishProgress(content_after_translate);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String value = values[0];
            tvContent.setText(value);
        }
    }

}

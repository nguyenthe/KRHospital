package com.example.hongngoc.webview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.modelObject.DBHelper;
import com.example.hongngoc.webview.modelObject.Sym;
import com.example.hongngoc.webview.modelObject.SymtomAdapter;

import java.util.ArrayList;

/**
 * Created by HongNgoc on 9/11/2016.
 */
public class MainResult extends Activity {
    public static String time_since = "";
    public static String time_frequency = "";
    public static String content_part_body = "";
    public static String id_partment = "";

    private TextView tvParent;
    private TextView tvTimeSince;
    private TextView tvTimeFrequency;
    private ArrayList<Sym> list_Sym = new ArrayList<Sym>();
    private DBHelper db;
    //list symptom and image Button
    private SymtomAdapter adapter = null;
    private ListView listView;
    public Common common;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(this);
        common = new Common();
        setContentView(R.layout.layoutresult);
        getViewFromLayout();
        getContentIntent();
        showInfo();
        handler();
        deleteItem();
    }

    public void getViewFromLayout(){
        tvParent = (TextView)findViewById(R.id.tvNameParent);
        tvTimeSince = (TextView)findViewById(R.id.tvTimeSince);
        tvTimeFrequency = (TextView)findViewById(R.id.tvTimeFrequency);
        listView = (ListView)findViewById(R.id.listSymptom);
    }

    public void getContentIntent(){
        Intent intent = getIntent();
        id_partment =  intent.getStringExtra("id_department");
        time_since = intent.getStringExtra("time_since");
        content_part_body =  intent.getStringExtra("name_part");
        time_frequency = intent.getStringExtra("time_frequency");
        System.out.println("Time at MainResult : " + time_since + "---" + time_frequency);
    }

    public void showInfo() {
        int quantity_since = db.numberOfRows(db.CONTACTS_TABLE_NAME_SINCE);
        int quantity_frequency = db.numberOfRows(db.CONTACTS_TABLE_NAME_FREQUENCY);

        if (quantity_since == 0) {
            Toast.makeText(MainResult.this,"You did not choice since when you sick", Toast.LENGTH_LONG).show();
        } else {
            Cursor cus = db.getOneSinceFromTableSince(Integer.parseInt(id_partment),common.getDate());
            if(!(cus.moveToFirst()) || cus.getCount() ==0){
                Toast.makeText(MainResult.this,"No data",Toast.LENGTH_LONG).show();
            }else {
                cus.moveToFirst();
                int id = cus.getInt(cus.getColumnIndex("id"));
                String time_since = cus.getString(cus.getColumnIndex("time_since"));
                System.out.println("Time Since of id_part : " + time_since);
                System.out.println("ID id table since" + Integer.toString(id));
                tvTimeSince.setText(time_since);
            }
        }
        //check quantity of table
        //if data < 1 then query just one row
        if (quantity_frequency == 0) {
            Toast.makeText(MainResult.this, "You didnot choice frequency ", Toast.LENGTH_LONG).show();
        } else {
            Cursor cus_fre = db.getOneSinceFromTableFrequency(Integer.parseInt(id_partment), common.getDate());
            if (!(cus_fre.moveToFirst()) || cus_fre.getCount() ==0) {
                Toast.makeText(MainResult.this, "No data", Toast.LENGTH_LONG).show();
                //tvTimeSince.setText("");
            } else {
                cus_fre.moveToFirst();
                int id_fre = cus_fre.getInt(cus_fre.getColumnIndex("id"));
                String time_fre = cus_fre.getString(cus_fre.getColumnIndex("time_frequency"));
                if (time_fre == null) {
                    tvTimeFrequency.setText("");
                } else {
                    tvTimeFrequency.setText(time_fre);
                }
            }
        }
    }

    public void newIntent(Class b,String context){
        Intent intent = new Intent(MainResult.this,b);
        intent.putExtra("message",context);
        startActivity(intent);
    }

    public void deleteItem(){
        for(int i=listView.getChildCount()-1;i>=0;i--){
            View v = listView.getChildAt(i);
            ImageButton chk = (ImageButton) v.findViewById(R.id.btnDestroy);
            chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainResult.this,"Lam gi thi lam",Toast.LENGTH_LONG).show();
                }
            });
        }

        adapter.notifyDataSetChanged();
    }

    public void handler(){
        System.out.println("Id part of MainResult Class : " +id_partment);
        System.out.println("Current time MainResult Class : " + common.getDate());
        list_Sym = (ArrayList<Sym>) db.getAllSymtpmByIdPart(Integer.parseInt(id_partment),common.getDate());
        if(list_Sym.size()==0){
            Toast.makeText(MainResult.this,"Nothing Data In here !!! ",Toast.LENGTH_LONG).show();
        }

        /*adapter = new SymtomAdapter
                (MainResult.this,R.layout.item_final,list_Sym);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                final Sym item = (Sym)adapter.getItem(position);
                Dialog d = new AlertDialog.Builder(MainResult.this,AlertDialog.THEME_HOLO_LIGHT)
                        .setTitle("Symptom you selected!!!")
                        .setNegativeButton("Cancel", null)
                        .setItems(new String[]{item.getContent()}, new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dlg, int position)
                            {
                                if(position == 0)
                                {
                                    newIntent(Translate.class,item.getContent());
                                }
                            }
                        })
                        .create();
                d.show();

            }
        });*/

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                final int asaa = position;
                Dialog d = new AlertDialog.Builder(MainResult.this,AlertDialog.THEME_HOLO_LIGHT)
                        .setTitle("Symptom you selected!!!")
                        .setNegativeButton("Cancel", null)
                        .setItems(new String[]{"Yes","No"}, new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dlg, int position)
                            {
                                if(position == 0)
                                {
                                   list_Sym.remove(asaa);
                                    adapter.notifyDataSetChanged();
                                }else{
                                   dlg.dismiss();
                                }
                            }
                        })
                        .create();
                d.show();


                return true;
            }
        });

    }

}

package com.example.hongngoc.webview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.net.ConnectivityManager;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by HongNgoc on 9/20/2016.
 */
public class MainWebView extends Activity {
    final String URL = "https://192.168.1.25:8080/";
    //final String URL = "https://google.com";
    private WebView webView;
    private Button btnSend;
    private static final int REQUEST_CAMERA_RESULT = 1;
    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    //@SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = (WebView) findViewById(R.id.webViewMain);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        //webView.addJavascriptInterface(new WebViewJavaScriptInterface(this), "app");
        webView.setWebChromeClient(new WebChromeClient() {
            /* @Override
             public void onPermissionRequest(PermissionRequest request) {
                 // Show a grant or deny dialog to the user
                 // On accept or deny call
                 request.grant(request.getResources()) ;
                 // or
                 // request.deny()
             }*/
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                System.out.println("|> onPermissionRequest");
                MainWebView.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        System.out.println("|> onPermissionRequest run");
                        request.grant(request.getResources());
                    }// run
                });// MainActivity

            }// onPermissionRequest
        });
        webView.setWebViewClient(new SSLTolerentWebViewClient());

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                webView.getSettings().setLoadsImagesAutomatically(true);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                webView.loadUrl(URL);
            }
        });
    }


    private class SSLTolerentWebViewClient extends WebViewClient {
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }

    }

    public class WebViewJavaScriptInterface {
        private Context context;
        public WebViewJavaScriptInterface(Context context) {
            this.context = context;
        }
        @JavascriptInterface
        public void makeToast(String message, boolean lengthLong) {
            Toast.makeText(context, message, (lengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            webView.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            super.onReceivedSslError(view, handler, error);
            handler.proceed();
        }
    }
}

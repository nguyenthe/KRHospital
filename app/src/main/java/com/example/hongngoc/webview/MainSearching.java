package com.example.hongngoc.webview;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.modelObject.Symptom;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HongNgoc on 9/30/2016.
 */
public class MainSearching extends Activity implements TextWatcher {
    private TextView tvSelection;
    private MultiAutoCompleteTextView multiComplete;
    private ArrayList<Symptom>  arrayObject = null;
    private ArrayList<String> list_content = new ArrayList<String>();
    AutoCompleteTextView singleComplete;
    String arr[]={"hà nội","Huế","Sài gòn",
            "hà giang","Hội an","Kiên giang",
            "Lâm đồng","Long khánh"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_searching);
        getDataSet(1);
        getLayout();
        handlerMultiSearching();
    }

    public void getLayout(){
        tvSelection = (TextView)findViewById(R.id.selection_Content_Symptom);
        multiComplete = (MultiAutoCompleteTextView)findViewById(R.id.multiAutoCompleteTextView1);
    }

    private void getDataSet(int idx) {
        AsyncTaskSymtom loader = new AsyncTaskSymtom();
        System.out.println("getDataSet from Main Searching: " + idx);
        AsyncTask<String, String, List<Symptom>> result = loader.execute("" + idx);
        try {
            //mai thao cai comment nay ra
            arrayObject = (ArrayList<Symptom>) result.get();
            System.out.println("Size demo  tabf1" + arrayObject.size());
            for(int i=0;i<arrayObject.size();i++){
                list_content.add(arrayObject.get(i).getContent_en());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void handlerMultiSearching(){
        //Thiết lập ArrayADapter
        multiComplete.setAdapter(
                new ArrayAdapter<String>
                        (
                                this,
                                android.R.layout.simple_list_item_1,
                                list_content
                        ));
        //Đối với MultiAutoCompleteTextView bắt buộc phải gọi dòng lệnh này
        multiComplete.setTokenizer(new MultiAutoCompleteTextView
                .CommaTokenizer());
    }

    //Khi chọn trong AutoCompleteTextView hàm này sẽ tự động phát sinh
    public void onTextChanged(CharSequence arg0, int arg1,
                              int arg2, int arg3) {
        tvSelection.setText(multiComplete.getText());
    }
    public void afterTextChanged(Editable arg0) {
    }
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                  int arg3) {
    }

    public class AsyncTaskSymtom extends AsyncTask<String,String,List<Symptom>>{

        @Override
        protected List<Symptom> doInBackground(String... strings) {
            String content = "";
            try{
                //demo demo
                //String url = "http://192.168.1.25:8983/solr/select?q=*:*%20&fq={!join%20from=id_parent%20to=id_parent}id_parent:1";
                String url = Common.apache_url+":8983/solr/collection1/select?q=*%3A*&wt=json&indent=true";
                Document document1 = Jsoup.connect(url).get();
                Elements body = document1.select("body");
                System.out.println("The The The : " + body.text());
                String text_body = body.text().toString();
                System.out.println("TEXT TEXT : " + text_body);
                // contacts JSONArray

                JSONObject obj2 = new JSONObject(text_body);
                //System.out.println("Demo Demo Demo : " + obj);
                String demo = obj2.getString("response");
                System.out.println("Lenght : " + demo);

                JSONObject ob = new JSONObject(demo);
                System.out.println("OBOB"+ob);

                JSONArray jsonArray = ob.optJSONArray("docs");
                ArrayList<String> arrayList = new ArrayList<String>();
                arrayObject = new ArrayList<Symptom>();
                for(int i=0;i<jsonArray.length();i++){
                    System.out.println("iiiiiiiiiiiiiii");
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Symptom data = new Symptom();
                    data.setId_parent(Integer.parseInt(jsonObject.get("id_parent").toString()));
                    data.setId_dp(Integer.parseInt(jsonObject.get("id_dp").toString()));
                    data.setId_symptom(Integer.parseInt(jsonObject.get("id_symptom").toString()));
                    data.setContent_en(jsonObject.get("content_en").toString());
                    data.setContent_ko(jsonObject.get("content_ko").toString());
                    arrayObject.add(data);
                }

                for(int i=0;i<arrayObject.size();i++){
                    System.out.println("one by one element in array is : " + arrayObject.get(i).toString());
                }
                return arrayObject;
                //return null;
            }catch(Exception e){
                e.printStackTrace();
            }
            return arrayObject;
        }
    }
}

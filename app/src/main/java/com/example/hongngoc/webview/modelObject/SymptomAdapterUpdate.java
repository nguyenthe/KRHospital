package com.example.hongngoc.webview.modelObject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.hongngoc.webview.R;

import java.util.ArrayList;

/**
 * Created by HongNgoc on 9/23/2016.
 */
public class SymptomAdapterUpdate extends RecyclerView.Adapter<SymptomAdapterUpdate.ViewHolder>  {
    private ArrayList<Symptom> mDataset;
    private  static OnItemClickListener mItemClickListener ;
    private static OnItemLongClickListener mItemLongClickListener;
    private static OnItemCheckListener onItemCheckListener;
    public static int position_need;
    public static ArrayList<Integer> list = new ArrayList<Integer>();
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        // each data item is just a string in this case
        public TextView txtContent;
        public TextView tv_Korean;
        public CheckBox checkbox;

        public ViewHolder(View v) {
            super(v);
            txtContent = (TextView) v.findViewById(R.id.contentSymptom);
            tv_Korean = (TextView)v.findViewById(R.id.contentSymptomKo);
            checkbox = (CheckBox)v.findViewById(R.id.checkboxItem);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(getPosition(),view);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if(mItemLongClickListener != null){
                mItemLongClickListener.onItemlongClick(getPosition(),view);
            }
            return true;
        }

    }

    public void remove(String item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public int getNumber(int x){
        return x;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SymptomAdapterUpdate(ArrayList<Symptom> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SymptomAdapterUpdate.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_choose, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Symptom symptom = mDataset.get(position);
        final String name = mDataset.get(position).getContent_en();
        holder.txtContent.setText(symptom.getContent_en());
        holder.tv_Korean.setText(symptom.getContent_ko());
        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((holder).checkbox.isChecked()) {
                    list.add(position);
                } else {
                    System.out.println("Nothing nothing checked");
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemCheckListener {
        void onItemCheck(int position,View view);
    }

    public void setOnItemCheckListener(OnItemCheckListener onItemCheckListener){
        this.onItemCheckListener = onItemCheckListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(int position,View view);
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    public interface OnItemLongClickListener {
        public void onItemlongClick(int position,View v);
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener){
        this.mItemLongClickListener = onItemLongClickListener;
    }

}

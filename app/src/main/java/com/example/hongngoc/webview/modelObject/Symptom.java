package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/9/2016.
 */
public class Symptom {
    private int id_symptom;
    private int id_dp;
    private int id_parent;
    private String content_en;
    private String content_ko;

    public Symptom() {

    }

    public int getId_symptom() {
        return id_symptom;
    }

    public void setId_symptom(int id_symptom) {
        this.id_symptom = id_symptom;
    }

    public int getId_dp() {
        return id_dp;
    }

    public void setId_dp(int id_dp) {
        this.id_dp = id_dp;
    }

    public int getId_parent() {
        return id_parent;
    }

    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    public String getContent_en() {
        return content_en;
    }

    public void setContent_en(String content_en) {
        this.content_en = content_en;
    }

    public String getContent_ko() {
        return content_ko;
    }

    public void setContent_ko(String content_ko) {
        this.content_ko = content_ko;
    }

    public Symptom(int id_symptom, int id_dp, int id_parent, String content_en, String content_ko) {
        this.id_symptom = id_symptom;
        this.id_dp = id_dp;
        this.id_parent = id_parent;
        this.content_en = content_en;
        this.content_ko = content_ko;
    }

    @Override
    public String toString() {
        return "Symptom{" +
                "id_symptom=" + id_symptom +
                ", id_dp=" + id_dp +
                ", id_parent=" + id_parent +
                ", content_en='" + content_en + '\'' +
                ", content_ko='" + content_ko + '\'' +
                '}';
    }
}

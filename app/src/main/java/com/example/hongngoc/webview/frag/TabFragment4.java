package com.example.hongngoc.webview.frag;

/**
 * Created by HongNgoc on 9/23/2016.
 */

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.modelObject.DBHelper;
import com.example.hongngoc.webview.modelObject.Symptom;
import com.example.hongngoc.webview.translate.TTSManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TabFragment4 extends Fragment implements TextWatcher {

    DBHelper db;
    private Button speakNowButton;
    private EditText editText;
    private ImageButton btnVoice;
    private TextView textView;
    private ArrayList<Symptom>  arrayObject = null;
    private ArrayList<String> list_content = new ArrayList<String>();
    TTSManager ttsManager = null;
    private static final int REQUEST_CODE = 123;
    private final int SPEECH_RECOGNITION_CODE = 1;
    Dialog match_text_dialog;
    ListView textlist;
    ArrayList<String> matches_text;
    private static String content= "";
    private MultiAutoCompleteTextView multiComplete;
    Context context; //Activity
    private static String content_symptom;
    String result_language;
    String content_after_translate;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            content_symptom = getArguments().getString("symptom");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(content_symptom == null){
            multiComplete.setText("");
        }else
            multiComplete.setText(content_symptom);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //define context for Activity
        context = container.getContext();
        View view = inflater.inflate(R.layout.translate, container, false);
        ttsManager = new TTSManager();
        ttsManager.init(context);
        getComponentFromLayout(view);
        getDataSet(1);
        handlerMultiSearching();
        btnVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()){
                    startSpeechToText();
                }
                else{
                    Toast.makeText(context.getApplicationContext(), "Please Connect to Internet", Toast.LENGTH_LONG).show();
                }}
        });

        speakNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String text = multiComplete.getText().toString();
                ttsManager.initQueue(text);
            }
        });
        return view;
    }

    public  boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public void getComponentFromLayout(View view){
        //editText = (EditText) view.findViewById(R.id.input_text);
        speakNowButton = (Button) view.findViewById(R.id.speak_now);
        textView = (TextView)view.findViewById(R.id.textView);
        btnVoice = (ImageButton)view.findViewById(R.id.btn_voice);
        multiComplete = (MultiAutoCompleteTextView)view.findViewById(R.id.multiAutoCompleteTranslate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SPEECH_RECOGNITION_CODE: {
                if (resultCode == getActivity().RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = result.get(0);
                    textView.setText(text);
                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void handlerMultiSearching(){
        //Thiết lập ArrayADapter
        multiComplete.setAdapter(
                new ArrayAdapter<String>
                        (
                                getActivity(),
                                android.R.layout.simple_list_item_1,
                                list_content
                        ));
        //Đối với MultiAutoCompleteTextView bắt buộc phải gọi dòng lệnh này
        multiComplete.setTokenizer(new MultiAutoCompleteTextView
                .CommaTokenizer());
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        multiComplete.setText(multiComplete.getText());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void startSpeechToText() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speak something...");
        try {
            startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(),
                    "Sorry! Speech recognition is not supported in this device.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public class CheckDetectionLanguage extends AsyncTask<String,String,String>{
        @Override
        protected String doInBackground(String... strings) {
            try {
                String result_voice = strings[0];
                //detection language
                String check_language = "https://www.googleapis.com/language/translate/v2/detect?key=AIzaSyCStJnRPJTip-UhZHHSz8ly8QDoV0ahACs&q="+result_voice;
                Document document_getName_1 = Jsoup.connect(check_language).ignoreContentType(true).get();
                System.out.println("format html " + document_getName_1.toString());
                Elements body_name_1 = document_getName_1.select("body");

                String text_body_1 = body_name_1.text().toString();
                System.out.println("TEXT TEXT : " + text_body_1);

                JSONObject obj2_1 = new JSONObject(text_body_1);
                String demo_1 = obj2_1.getString("data");
                System.out.println("data from json : " + demo_1);

                JSONObject obj3_1 = new JSONObject(demo_1);
                String demo1_1 = obj3_1.getString("detections");

                JSONArray jsonArray_1 = obj3_1.getJSONArray("detections");
                System.out.println("aaaaa111" + jsonArray_1.length());

                for (int i = 0; i < jsonArray_1.length(); i++) {
                    JSONArray jsonArray = jsonArray_1.getJSONArray(0);
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        result_language = jsonObject.getString("language");
                        System.out.println("AAAAA translate translate : " + result_language);
                        publishProgress(result_language);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            //return arrayPartBody;
            return result_language;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Toast.makeText(context,"AAAAAAAAAAAAAAAA"+values[0],Toast.LENGTH_LONG).show();
            if(values[0].equals("und")){
                Toast.makeText(context,"Sorry We cant understading !!!",Toast.LENGTH_LONG).show();
            }
            AsytaskTranslate an = new AsytaskTranslate();
            an.execute(values[0]);
        }
    }

    public void handlerTranslate(String values){
        switch (values){
            case "en":
                AsytaskTranslate asytaskTranslate = new AsytaskTranslate();
                break;
            case "ko":
                AsytaskTranslate asytaskTranslate1 = new AsytaskTranslate();
                break;
        }
    }

    public class AsytaskTranslate extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... strings) {
            String content = strings[0];
            try {
                System.out.println("Content will handler translate  ...!!!" + content);
                String url3 = "https://www.googleapis.com/language/translate/v2?key=AIzaSyCStJnRPJTip-UhZHHSz8ly8QDoV0ahACs&q="+content+"&source=en&target=ko";
                Document document_getName = Jsoup.connect(url3).ignoreContentType(true).get();
                System.out.println("format html " + document_getName.toString());
                Elements body_name = document_getName.select("body");

                String text_body = body_name.text().toString();
                System.out.println("TEXT TEXT : " + text_body);

                JSONObject obj2 = new JSONObject(text_body);
                String demo = obj2.getString("data");
                System.out.println("data from json : " + demo);

                JSONObject obj3 = new JSONObject(demo);
                String demo1 = obj3.getString("translations");

                JSONArray jsonArray = obj3.getJSONArray("translations");
                System.out.println("aaaaa" + jsonArray.length());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    content_after_translate = jsonObject.getString("translatedText");
                    System.out.println("AAAAA " + content_after_translate);
                    publishProgress(content_after_translate);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String value = values[0];
            textView.setText(value);
        }
    }


    private void getDataSet(int idx) {
        AsyncTaskSymtom loader = new AsyncTaskSymtom();
        System.out.println("getDataSet from Main Searching: " + idx);
        AsyncTask<String, String, List<Symptom>> result = loader.execute("" + idx);
        try {
            //mai thao cai comment nay ra
            arrayObject = (ArrayList<Symptom>) result.get();
            System.out.println("Size demo  tabf1" + arrayObject.size());
            for(int i=0;i<arrayObject.size();i++){
                list_content.add(arrayObject.get(i).getContent_ko());
            }

            for(int i=0;i<list_content.size();i++){
                list_content.add(arrayObject.get(i).getContent_en());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class AsyncTaskSymtom extends AsyncTask<String,String,List<Symptom>> {

        @Override
        protected List<Symptom> doInBackground(String... strings) {
            String content = "";
            try {
                //demo demo
                //String url = "http://192.168.1.25:8983/solr/select?q=*:*%20&fq={!join%20from=id_parent%20to=id_parent}id_parent:1";
                String url = Common.apache_url+":8983/solr/collection1/select?q=*%3A*&wt=json&indent=true";
                Document document1 = Jsoup.connect(url).get();
                Elements body = document1.select("body");
                System.out.println("The The The : " + body.text());
                String text_body = body.text().toString();
                System.out.println("TEXT TEXT : " + text_body);
                // contacts JSONArray
                JSONObject obj2 = new JSONObject(text_body);
                //System.out.println("Demo Demo Demo : " + obj);
                String demo = obj2.getString("response");
                System.out.println("Lenght : " + demo);
                JSONObject ob = new JSONObject(demo);
                System.out.println("OBOB" + ob);

                JSONArray jsonArray = ob.optJSONArray("docs");
                ArrayList<String> arrayList = new ArrayList<String>();
                arrayObject = new ArrayList<Symptom>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    System.out.println("iiiiiiiiiiiiiii");
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Symptom data = new Symptom();
                    data.setId_parent(Integer.parseInt(jsonObject.get("id_parent").toString()));
                    data.setId_dp(Integer.parseInt(jsonObject.get("id_dp").toString()));
                    data.setId_symptom(Integer.parseInt(jsonObject.get("id_symptom").toString()));
                    data.setContent_en(jsonObject.get("content_en").toString());
                    data.setContent_ko(jsonObject.get("content_ko").toString());
                    arrayObject.add(data);
                }

                for (int i = 0; i < arrayObject.size(); i++) {
                    System.out.println("one by one element in array is : " + arrayObject.get(i).toString());
                }
                return arrayObject;
                //return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return arrayObject;
        }
    }
}
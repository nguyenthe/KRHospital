package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/11/2016.
 */
public class Frequency {
    public int id_frequency;
    public String time_frequency;
    public int id_part;
    public String date_info;

    public int getId_frequency() {
        return id_frequency;
    }

    public void setId_frequency(int id_frequency) {
        this.id_frequency = id_frequency;
    }

    public String getTime_frequency() {
        return time_frequency;
    }

    public void setTime_frequency(String time_frequency) {
        this.time_frequency = time_frequency;
    }

    public int getId_part() {
        return id_part;
    }

    public void setId_part(int id_part) {
        this.id_part = id_part;
    }

    public String getDate_info() {
        return date_info;
    }

    public void setDate_info(String date_info) {
        this.date_info = date_info;
    }
    public Frequency(){}

    public Frequency(int id_frequency, String time_frequency, int id_part, String date_info) {
        this.id_frequency = id_frequency;
        this.time_frequency = time_frequency;
        this.id_part = id_part;
        this.date_info = date_info;
    }

    @Override
    public String toString() {
        return "Frequency{" +
                "id_frequency=" + id_frequency +
                ", time_frequency='" + time_frequency + '\'' +
                ", id_part=" + id_part +
                ", date_info='" + date_info + '\'' +
                '}';
    }
}

package com.example.hongngoc.webview.modelObject;

/**
 * Created by HongNgoc on 9/25/2016.
 */
public class Final {
    private static int count = 0;
    private int id;
    private String title;   //name part
    private String content; //symptom
    private String since_time; //time since
    private String frequency_time;
    private String date;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSince_time() {
        return since_time;
    }

    public void setSince_time(String since_time) {
        this.since_time = since_time;
    }

    public String getFrequency_time() {
        return frequency_time;
    }

    public void setFrequency_time(String frequency_time) {
        this.frequency_time = frequency_time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Final(String title, String content, String since_time, String frequency_time, String date,String image) {
        this.title = title;
        this.content = content;
        this.since_time = since_time;
        this.frequency_time = frequency_time;
        this.date = date;
        this.image = image;
    }

    public Final(){
        id = ++count;
    }

    @Override
    public String toString() {
        return "Final{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", since_time='" + since_time + '\'' +
                ", frequency_time='" + frequency_time + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}

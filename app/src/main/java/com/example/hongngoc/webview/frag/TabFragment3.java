package com.example.hongngoc.webview.frag;

/**
 * Created by HongNgoc on 9/23/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.hongngoc.webview.MainTab;
import com.example.hongngoc.webview.R;
import com.example.hongngoc.webview.modelObject.DBHelper;

public class TabFragment3 extends Fragment {

    private TextView textView;
    private NumberPicker np;
    private RadioGroup radioGroup;
    private RadioButton rbHour;
    private RadioButton rbDay;
    private RadioButton rbMonth;
    private RadioButton rbYear;
    private RadioButton radio;
    private TextView tvContent;
    private static String content = "";
    private static String id_department = "";
    private static String time_since = "";
    private static String time_frequency = "";
    private String  content_part_body = "";
    DBHelper db;
    Context context; //Activity
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //define context for Activity
        context = container.getContext();
        View view = inflater.inflate(R.layout.frequency, container, false);
        handlerRadioButton(view);
        return view;
    }

    public static TabFragment3 newInstance(String text){
        TabFragment3 f = new TabFragment3();
        Bundle b = new Bundle();
        b.putString("msg","this is from tab1");
        f.setArguments(b);
        return f;
    }

    public void handlerGetIntent(){
        // Create object of SharedPreferences.
        Intent intent = getActivity().getIntent();
        id_department = intent.getStringExtra("id_department");
        time_since = intent.getStringExtra("time_since");
        time_frequency = intent.getStringExtra("time_frequency");
        content_part_body =  intent.getStringExtra("name_part");
        System.out.println("MainSince get id_department : " + id_department +"name_part"+content_part_body+"--"+ "time-since"+time_since
                + "time-frequency" +time_frequency);
    }

    public void handlerRadioButton(View view){
        np = (NumberPicker)view.findViewById(R.id.np);
        np.setMinValue(0);
        np.setMaxValue(20);
        //gets whether the selector wheel when reaching the min/max value
        np.setWrapSelectorWheel(true);
        radioGroup = (RadioGroup)view.findViewById(R.id.radioGroup);
        int idChecked = radioGroup.getCheckedRadioButtonId();
        rbHour = (RadioButton)view.findViewById(R.id.radioButtonFtHours);
        rbDay = (RadioButton)view.findViewById(R.id.radioButtonFtDay);
        rbMonth = (RadioButton)view.findViewById(R.id.radioButtonFtMonth);
        rbYear = (RadioButton)view.findViewById(R.id.radioButtonFtYear);
        radio = (RadioButton)radioGroup.findViewById(idChecked);
        tvContent = (TextView)view.findViewById(R.id.totalTime);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newValue) {
                //textView.setText("Selected Number : " +newValue);
                if(rbHour.isChecked()) {
                    content = Integer.toString(newValue) + "  hours/time" + "\n";
                    System.out.println("Value : " + content);
                    tvContent.setText(content);
                    MainTab.myBundle.putString("time_frequency",content);
                }
                if(rbDay.isChecked()) {
                    content = Integer.toString(newValue) + "  day/time" + "\n";
                    System.out.println("Value : " + content);
                    tvContent.setText(content);
                    MainTab.myBundle.putString("time_frequency",content);
                }
                if(rbMonth.isChecked()) {
                    content = Integer.toString(newValue) + "  month/time" + "\n";
                    System.out.println("Value : " + content);
                    tvContent.setText(content);
                    MainTab.myBundle.putString("time_frequency",content);
                }
                if(rbYear.isChecked()) {
                    content = Integer.toString(newValue) + "  Year/time" + "\n";
                    System.out.println("Value : " + content);
                    tvContent.setText(content);
                    MainTab.myBundle.putString("time_frequency",content);
                }
            }
        });
    }
}
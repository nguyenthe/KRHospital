package com.example.hongngoc.webview;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hongngoc.webview.common.Common;
import com.example.hongngoc.webview.handlerListView.ToolbarHidingOnScrollListener;
import com.example.hongngoc.webview.modelObject.ListItemAdapter;
import com.example.hongngoc.webview.modelObject.PartBody;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HongNgoc on 9/22/2016.
 */

public class MainListItem extends ActionBarActivity {

    private static String content_part_body = "";
    private static String id_Part_Body = "";
    private ArrayList<PartBody>  arrayPartBody = new ArrayList<PartBody>();
    private RecyclerView recyclerView;
    private ListItemAdapter mAdapter;
    private static FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.itemviewpart_update);
        fragmentManager = getSupportFragmentManager();
        setupToolbar();
        getDataSet(1);
    }

    private void getDataSet(int idx) {
        ConnectionToGetData loader = new ConnectionToGetData();
        System.out.println("getDataSet from mainListItem : " + idx);
        AsyncTask<String, String, List<PartBody>> result = loader.execute("" + idx);
        try {
            mAdapter = new ListItemAdapter(this,
                    (ArrayList<PartBody>) result.get());
            recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
            // Let's use a grid with 2 columns.
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            recyclerView.setAdapter(mAdapter);
            View tabBar = findViewById(R.id.fake_tab);
            View coloredBackgroundView = findViewById(R.id.colored_background_view);
            View toolbarContainer = findViewById(R.id.toolbar_container);
            View toolbar = findViewById(R.id.toolbar);
            recyclerView.setOnScrollListener(new ToolbarHidingOnScrollListener(toolbarContainer, toolbar, tabBar, coloredBackgroundView));
            System.out.println("Ket qua Ket qua "+ result.toString());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        (mAdapter).SetOnItemClickListener(new ListItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                Toast.makeText(MainListItem.this,"LLLL"+position,Toast.LENGTH_LONG).show();
                PartBody item = (PartBody) mAdapter.getItem(position);
                Toast.makeText(MainListItem.this,item.getName_en().toString(),Toast.LENGTH_LONG).show();
                //Send data from Activity to fragment
                /*Fragment app1 = new TabFragment1();
                Bundle bundle = new Bundle();
                bundle.putString("image","hello");
                app1.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.mainList,app1).commit();*/
                Intent intent = new Intent(MainListItem.this,MainTab.class);
                intent.putExtra("id_department",Integer.toString(item.getId_part()));
                intent.putExtra("message",item.getImage());
                System.out.println("mainListItem id_department : " + Integer.toString(item.getId_part()));
                intent.putExtra("name_part",content_part_body);
                startActivity(intent);

            }
        });
    }


    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public class ConnectionToGetData extends AsyncTask<String,String,List<PartBody>>{
        @Override
        protected List<PartBody>  doInBackground(String... strings) {
            Intent intent = getIntent();
            content_part_body = intent.getStringExtra("message");
            String content = "";
            try{
                String url3 = Common.url_spring+":8080/getIdByName/"+content_part_body;
                Document document_getName = Jsoup.connect(url3).get();
                System.out.println("Name Name name " + document_getName.toString());
                Elements body_name = document_getName.select("body");
                id_Part_Body = body_name.text();
                System.out.println("Name Body Name Body  : " + id_Part_Body);

                //String url2 = "http://192.168.0.30:8080/listPartsById/"+id_Part_Body;
                String url2 = Common.url_spring+":8080/listPartsById/"+id_Part_Body;
                Document document_demo = Jsoup.connect(url2).ignoreContentType(true).get();
                System.out.println("spring data : " + document_demo.text());
                JSONArray obj = new JSONArray(document_demo.text());
                ArrayList<PartBody> list_category = new ArrayList<PartBody>();
                for(int i=0;i<obj.length();i++){
                    JSONObject object = obj.getJSONObject(i);
                    System.out.println("abc "+object.get("name_en"));
                    PartBody body = new PartBody();
                    body.setId_parent(Integer.parseInt(object.get("id_parent").toString()));
                    body.setId_part(Integer.parseInt(object.get("id_dp").toString()));
                    body.setName_en(object.get("name_en").toString());
                    body.setName_ko(object.get("name_ko").toString());
                    body.setImage(object.get("image").toString());
                    arrayPartBody.add(body);
                }
                return arrayPartBody;
            }catch(Exception e){
                e.printStackTrace();
            }
            //return arrayPartBody;
            return arrayPartBody;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public void handlerOnePart(){
        for(int i=recyclerView.getChildCount()-1;i>=0;i--){
            View v = recyclerView.getChildAt(i);
            ImageView imageView = (ImageView)v.findViewById(R.id.image_list_item);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainListItem.this,"aaadadas",Toast.LENGTH_LONG).show();
                }
            });
        }

        /*mAdapter.SetOnItemClickListener(new ListItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                PartBody item = (PartBody) mAdapter.getItem(position);
                Toast.makeText(MainListItem.this,item.getName_en().toString(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainListItem.this,MainBasic.class);
                intent.putExtra("message",item.getImage());
                intent.putExtra("id_department",Integer.toString(item.getId_part()));
                intent.putExtra("name_part",content_part_body);
            }
        });*/

        /*recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemPosition = recyclerView.getChildLayoutPosition(view);
                Toast.makeText(MainListItem.this,""+itemPosition, Toast.LENGTH_LONG).show();
                //int itemPosition = recyclerView.indexOfChild(view);
                System.out.println("Clicked and Position is "+String.valueOf(itemPosition));
                PartBody item = (PartBody) mAdapter.getItem(itemPosition);
                Toast.makeText(MainListItem.this,item.getName_en().toString(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainListItem.this,MainBasic.class);
                intent.putExtra("message",item.getImage());
                intent.putExtra("id_department",Integer.toString(item.getId_part()));
                intent.putExtra("name_part",content_part_body);
                //startActivity(intent);
            }
        });*/
    }

}

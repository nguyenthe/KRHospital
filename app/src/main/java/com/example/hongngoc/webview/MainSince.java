package com.example.hongngoc.webview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.hongngoc.webview.modelObject.DBHelper;
import com.example.hongngoc.webview.modelObject.Since;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by HongNgoc on 9/9/2016.
 */
public class MainSince extends Activity implements View.OnClickListener {

    private TextView textView;
    private NumberPicker np;
    private CheckBox cbDay;
    private CheckBox cbMonth;
    private CheckBox cbYear;
    private TextView tvContent;
    private static String content = "";
    private static String id_department = "";
    private static String time_since = "";
    private static String time_frequency = "";
    private ImageButton btnBack;
    private String  content_part_body = "";
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.since);
        db = new DBHelper(this);
        getViewLayout();
        handlerNumberPicket();
        handlerGetIntent();
    }

    public void getViewLayout(){
        /*np = (NumberPicker)findViewById(R.id.np);
        cbDay = (CheckBox)findViewById(R.id.checkBoxDay);
        cbMonth = (CheckBox)findViewById(R.id.checkBoxMonth);
        cbYear = (CheckBox)findViewById(R.id.checkBox3Year);
        tvContent = (TextView)findViewById(R.id.totalTime);
        //btnBack = (ImageButton)findViewById(R.id.btnSendSince);
        btnBack.setOnClickListener(this);*/
    }

    public void setColorForText(){
        textView.setTextColor(Color.parseColor("#ffd32b3b"));
    }

    public void handlerGetIntent(){
        // Create object of SharedPreferences.
        Intent intent = getIntent();
        id_department = intent.getStringExtra("id_department");
        time_since = intent.getStringExtra("time_since");
        time_frequency = intent.getStringExtra("time_frequency");
        content_part_body =  intent.getStringExtra("name_part");
        System.out.println("MainSince get id_department : " + id_department +"name_part"+content_part_body+"--"+ "time-since"+time_since
                + "time-frequency" +time_frequency);
    }

    public void handlerNumberPicket(){
        np.setMinValue(0);
        np.setMaxValue(20);
        //gets whether the selector wheel when reaching the min/max value
        np.setWrapSelectorWheel(true);

        //set value change listener for NumberPicket
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newValue) {
                //textView.setText("Selected Number : " +newValue);
                if(cbDay.isChecked()) {
                    content = Integer.toString(newValue) + "  day" + "\n";
                    tvContent.setText(content);
                }
                if(cbMonth.isChecked()) {
                    content = Integer.toString(newValue) + "  month" + "\n";
                    tvContent.setText(content);
                }
                if(cbYear.isChecked()) {
                    content = Integer.toString(newValue) + "  Year" + "\n";
                    tvContent.setText(content);
                }
            }
        });

    }

    @Override
    public void onClick(View view){
        Intent intent = new Intent(MainSince.this,MainBasic.class);
        intent.putExtra("time_since",content);
        intent.putExtra("id_department",id_department);
        intent.putExtra("time_frequency",time_frequency);
        intent.putExtra("name_part",content_part_body);
        Since since = new Since();
        since.setTime_since(content);
        since.setId_part(Integer.parseInt(id_department));
        Calendar c = Calendar.getInstance();
        System.out.println("Cureent time => " +c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        String formatDate = df.format(c.getTime());
        since.setDate_info(formatDate);
        db.insertTableSince(since);
        startActivity(intent);
    }
}
